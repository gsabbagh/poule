module Main (main) where

import          Parsers
import          SemanticAnalyser


main :: IO ()
main = do
    testParsers
    testSemanticAnalyser