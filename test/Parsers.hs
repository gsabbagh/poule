{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Test all parsers.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Test all poule parsers.
-}

module Parsers
(
    testParsers
)
where
    import                      Data.Void
    import                      Data.Text
    import                      Data.WeakSet
    import                      Text.Megaparsec
    import                      Language.Poule.Parser
    
    
    testParser :: (Eq a, Show a) => Parser a -> Text -> Either (ParseErrorBundle Text Void) a -> IO ()
    testParser parser input output
        | output == (runParser (parser <* eof) "" input) = putStrLn $ show input ++ " passed."
        | otherwise = error $ (show input) ++ " failed:\n"++ show output ++ " is different from " ++ show (runParser (parser <* eof) "" input)

    testParsers :: IO ()
    testParsers = do
        testParser parseSketchObject "a" (Right (Object (set []) "a"))
        testParser parseSketchObject "_abc" (Right (Object (set []) "_abc"))
        testParser parseSketchObject "a -> b -> c" (Right (Power (set []) (Object (set []) "a") (Power (set []) (Object (set []) "b") (Object (set []) "c"))))
        testParser parseSketchObject "a * b -> c" (Right (Product (set []) [Object (set []) "a",Power (set []) (Object (set []) "b") (Object (set []) "c")]))
        testParser parseSketchObject "(a&b) -> c" (Right (Power (set []) (Product (set []) [Object (set []) "a",Object (set []) "b"]) (Object (set []) "c")))
        testParser parseSketchObject "[a&b] -> (c + d)" (Right (Power (set []) (Product (set []) [Object (set []) "a",Object (set []) "b"]) (Coproduct (set []) [Object (set []) "c",Object (set []) "d"])))
        
        testParser parseSketchArrow "p[(a*b),1]" (Right (Projection (Product (set []) [Object (set []) "a",Object (set []) "b"]) 1))
        testParser parseSketchArrow "proj((a*b),1)" (Right (Projection (Product (set []) [Object (set []) "a",Object (set []) "b"]) 1))
        testParser parseSketchArrow "coproj[(a*b),1]" (Right (Coprojection (Product (set []) [Object (set []) "a",Object (set []) "b"]) 1))
        testParser parseSketchArrow "q((a*b),1)" (Right (Coprojection (Product (set []) [Object (set []) "a",Object (set []) "b"]) 1))
        testParser parseSketchArrow "eval((a*b))" (Right (EvalMap (Product (set []) [Object (set []) "a",Object (set []) "b"])))
        testParser parseSketchArrow "e((a*b))" (Right (EvalMap (Product (set []) [Object (set []) "a",Object (set []) "b"])))
        
        testParser parseSketchComposition "p((a*b),1) >> f" (Right [Projection (Product (set []) [Object (set []) "a",Object (set []) "b"]) 1,ElementaryArrow "f"])
        testParser parseSketchComposition "p((a*b),1) << f" (Right [ElementaryArrow "f",Projection (Product (set []) [Object (set []) "a",Object (set []) "b"]) 1])
        testParser parseSketchComposition "f" (Right [ElementaryArrow "f"])
        testParser parseSketchComposition "id" (Right [])

        testParser parseSketchStatement "test" (Right (NewObject (Object (set []) "test")))
        testParser parseSketchStatement "test : a -> (b + c)" (Right (NewArrow (ElementaryArrow "test") (Object (set []) "a") (Coproduct (set []) [Object (set []) "b",Object (set []) "c"])))
        testParser parseSketchStatement "f >> g = h >> i" (Right (LawEntry [ElementaryArrow "f",ElementaryArrow "g"] [ElementaryArrow "h",ElementaryArrow "i"]))
        testParser parseSketchStatement "f >> g := i << h" (Right (LawEntry [ElementaryArrow "f",ElementaryArrow "g"] [ElementaryArrow "h",ElementaryArrow "i"]))
        testParser parseSketchStatement "prod alias a*b" (Right (TypeAlias "prod" (Product (set []) [Object (set []) "a",Object (set []) "b"])))
        testParser parseSketchStatement "test eq a*b (f1,f2) (f3,f4)" (Right (MultiEqualizer (Object (set []) "test") (Product (set []) [Object (set []) "a",Object (set []) "b"]) [([ElementaryArrow "f1"],[ElementaryArrow "f2"]),([ElementaryArrow "f3"],[ElementaryArrow "f4"])]))
        testParser parseSketchStatement "test equalizer a*b (f1,f2) (f3,f4)" (Right (MultiEqualizer (Object (set []) "test") (Product (set []) [Object (set []) "a",Object (set []) "b"]) [([ElementaryArrow "f1"],[ElementaryArrow "f2"]),([ElementaryArrow "f3"],[ElementaryArrow "f4"])]))
        testParser parseSketchStatement "test multiequalizer a*b (f1,f2) (f3,f4)" (Right (MultiEqualizer (Object (set []) "test") (Product (set []) [Object (set []) "a",Object (set []) "b"]) [([ElementaryArrow "f1"],[ElementaryArrow "f2"]),([ElementaryArrow "f3"],[ElementaryArrow "f4"])]))
        testParser parseSketchStatement "test multicoequalizer a*b (f1,f2) (f3,f4)" (Right (MultiCoequalizer (Object (set []) "test") (Product (set []) [Object (set []) "a",Object (set []) "b"]) [([ElementaryArrow "f1"],[ElementaryArrow "f2"]),([ElementaryArrow "f3"],[ElementaryArrow "f4"])]))
        testParser parseSketchStatement "instance test { a -> b; f => g;}" (Right (Instance "test" [MapObject (Object (set []) "a") (Object (set []) "b"),MapArrow (ElementaryArrow "f") [ElementaryArrow "g"]]))
        
        testParser parseSketchDeclaration "sketch test{a; f:a -> b; f >> g = h >>i; prod alias (a&b); test eq a*b (a,b); instance test { a -> b; f => g}}" (Right (Sketch (SketchDeclaration "test" [NewObject (Object (set []) "a"),NewArrow (ElementaryArrow "f") (Object (set []) "a") (Object (set []) "b"),LawEntry [ElementaryArrow "f",ElementaryArrow "g"] [ElementaryArrow "h",ElementaryArrow "i"],TypeAlias "prod" (Product (set []) [Object (set []) "a",Object (set []) "b"]),MultiEqualizer (Object (set []) "test") (Product (set []) [Object (set []) "a",Object (set []) "b"]) [([ElementaryArrow "a"],[ElementaryArrow "b"])],Instance "test" [MapObject (Object (set []) "a") (Object (set []) "b"),MapArrow (ElementaryArrow "f") [ElementaryArrow "g"]]])))
        
        testParser parseSketchMorphismDeclaration "smorphism test inclusion (a,b)" (Right (SketchMorphism (SketchMorphismInclusion "test" "a" "b")))
        testParser parseSketchMorphismDeclaration "smorphism test (a,b) {a -> b; f => g;}" (Right (SketchMorphism (SketchMorphismDeclaration "test" "a" "b" [MapObject (Object (set []) "a") (Object (set []) "b"),MapArrow (ElementaryArrow "f") [ElementaryArrow "g"]])))
        
        testParser parseModelDeclaration "model test (sk1,Haskell) instance a b c; bla bla; bla bla;end model" (Right (Model (ModelDeclaration "test" "sk1" Haskell ["a","b","c"] "; bla bla; bla bla;")))
        
        testParser parseImportDeclaration "import \"test\"" (Right (Import (ImportHidingStatement "test" [])))
        testParser parseImportDeclaration "from \"test\" import test" (Right (Import (FromImportAsStatement "test" "test" "test")))
        testParser parseImportDeclaration "from \"test\" import test as test2" (Right (Import (FromImportAsStatement "test" "test" "test2")))
        testParser parseImportDeclaration "from \"test\" import test as test2" (Right (Import (FromImportAsStatement "test" "test" "test2")))
        
        testParser parsePoule "import \"test\"; sketch test{a; b};" (Right ([Import (ImportHidingStatement "test" []),Sketch (SketchDeclaration "test" [NewObject (Object (set []) "a"),NewObject (Object (set []) "b")])]))