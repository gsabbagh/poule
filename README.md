# poule


A programming language based on sketch theory.

The programming language is very syntactic and allows to program with specifications. As long as functions defined are unique up to isomorphism, the compilation process can be done.

The compiler allows to compute the inverse of the precomposition functor associated to a sketch morphism in three different target language (Haskell, C and Python).


## About

This is a user friendly reboot of the [falcon programming language](https://gitlab.utc.fr/gsabbagh/falcon).