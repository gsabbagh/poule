{-| Module  : falcon
Description : Main program for the Poule compiler.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Main program for the Poule compiler.

-}


module Main 
(
    main
) 

where

    import          Options.Applicative
    import          Language.Poule.Compiler
    import          Math.IO.PrettyPrint

    main :: IO ()
    main = do
        options <- execParser parseOptionsCompilerWithInfo
        maybeError <- compile options
        if null maybeError then return () else do
            let Just err = maybeError
            pp 10 err
            if verbosity options > 1 then do
                putStrLn $ show err
            else do
                return ()
