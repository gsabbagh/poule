{-| Module  : poule
Description : Exports all transcompilers.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Exports all transcompilers.

-}

module Language.Poule.Transcompilers
(
    module Language.Poule.Transcompilers.Haskell,
    module Language.Poule.Transcompilers.C,
    module Language.Poule.Transcompilers.Python,
)

where
    import Language.Poule.Transcompilers.Haskell
    import Language.Poule.Transcompilers.C
    import Language.Poule.Transcompilers.Python