{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Precompiler for Poule.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Precompiler for Poule.

-}

module Language.Poule.Precompiler
(
    PrecompileError(..),
    -- * Roles
    ObjectRole(..),
    MorphismRole(..),
    getRolesOfObject,
    getRolesOfMorphism,
    -- * Determinacy
    ObjectDeterminacy(..),
    MorphismDeterminacy(..),
    getObjectDeterminacy,
    getMorphismDeterminacy,
    -- * Identifier creation
    objectToIdentifier,
    arrowToIdentifier,
    -- * Main function
    isSketchMorphismCompilable,
)

where
    import              Language.Poule.Parser  as P  hiding (Arrow)
    import              Language.Poule.SemanticAnalyser
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import              Data.List                   (intercalate)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    import qualified    Data.Text               as T
    
    import              GHC.Generics
    
    data PrecompileError =  WrongObjectDeterminacy (Set (SketchObject,ObjectDeterminacy))
                          | WrongMorphismDeterminacy (Set (CGMorphism SketchObject SketchArrow, MorphismDeterminacy))
                          | CompositionLawNotDeterminingAnyOfItsConstituents [Arrow SketchObject SketchArrow] [Arrow SketchObject SketchArrow]
        deriving (Show, Eq, Generic, Simplifiable, PrettyPrint)
    
    
    -- | The role of an object in the target of a sketch morphism. This role should be unique or be transported by the sketch morphism for the sketch morphism to be compilable.
    data ObjectRole = ImageObject SketchObject
                    | ApexObject (Cone (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject)
                    | NadirObject (Cocone (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject)
                    | PowerObject (Tripod (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject)
                    deriving (Show, Eq, Generic, Simplifiable, PrettyPrint)
    
    isImageObjectRole :: ObjectRole -> Bool
    isImageObjectRole (ImageObject _) = True
    isImageObjectRole _ = False
    
    -- | The role of a morphism in the target of a sketch morphism. This role should be unique or be transported by the sketch morphism for the sketch morphism to be compilable.    
    data MorphismRole = ImageMorphism (CGMorphism SketchObject SketchArrow)
                      | ConeLeg (Cone (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject) SketchObject
                      | CoconeLeg (Cocone (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject) SketchObject
                      | EvaluationMap (Tripod (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject)
                      | IdentityMorphism (CGMorphism SketchObject SketchArrow)
                      | CompositeMorphism [(CGMorphism SketchObject SketchArrow)]
                      | BindingCone (Cone (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject)
                      | BindingCocone (Cocone (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject)
                      | BindingTripod (Tripod (CompositionGraph SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) SketchObject) (CGMorphism SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) (CGMorphism SketchObject SketchArrow) -- ^ the target tripod and the source tripod power leg, domain leg and codomain leg
                      | CompositionLawEntry [Arrow SketchObject SketchArrow] [Arrow SketchObject SketchArrow]
                      deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
                      
                      
    isImageMorphismRole :: MorphismRole -> Bool
    isImageMorphismRole (ImageMorphism _) = True
    isImageMorphismRole _ = False
    
    -- | Given a sketch morphism and an object, return the set of roles of this object.
    getRolesOfObject :: SketchMorphism SketchObject SketchArrow -> SketchObject -> Set (ObjectRole)
    getRolesOfObject sm obj = imageRoles ||| apexRoles ||| nadirRoles ||| powerRoles
        where
            funct = underlyingFunctor sm
            imageRoles = [ImageObject k | (k,v) <- Map.mapToSet (omap funct), v == obj]
            apexRoles = [ApexObject c | c <- distinguishedCones (target sm), apex c == obj]
            nadirRoles = [NadirObject cc | cc <- distinguishedCocones (target sm), nadir cc == obj]
            powerRoles = [PowerObject t | t <- distinguishedTripods (target sm), powerObject t == obj]
    
    -- | Get the roles of a generating morphism or a generating leg of a (co)cone or any evaluation map.
    getRolesOfMorphism :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> Set (MorphismRole)
    getRolesOfMorphism sm morph = imageRoles ||| coneLegRoles ||| coconeLegRoles ||| evalMapRoles ||| identityRoles ||| bindingConeRoles ||| bindingCoconeRoles ||| bindingTripodRoles ||| compositeRoles
        where
            funct = underlyingFunctor sm
            morphismAntecedent m = [k | (k,v) <- Map.mapToSet (mmap funct), v == m]
            imageRoles
                | isIdentity (tgt funct) morph = [ImageMorphism (identity (src funct) k) | (k,v) <- Map.mapToSet (omap funct), v == (source morph)]
                | isGenerator (tgt funct) morph = ImageMorphism <$> morphismAntecedent morph
                | otherwise = ImageMorphism <$> (compose <$> (cartesianProductOfSets $ morphismAntecedent <$> decompose (tgt funct) morph))
            coneLegRoles = [ConeLeg c x | c <- distinguishedCones (target sm), x <- ob (indexingCategoryCone c), (legsCone c) =>$ x == morph]
            coconeLegRoles = [CoconeLeg cc x | cc <- distinguishedCocones (target sm), x <- ob (indexingCategoryCocone cc), (legsCocone cc) =>$ x == morph]
            evalMapRoles = [EvaluationMap t | t <- distinguishedTripods (target sm), (evalMap t) == morph]
            identityRoles
                | isIdentity (tgt funct) morph = set [IdentityMorphism morph]
                | otherwise = set []
            compositeRoles = if isComposite (tgt funct) morph then set [] else [CompositeMorphism (unsafeArrowToCGMorphism (tgt funct) <$> v) | (k,v) <- Map.mapToSet (law (tgt funct)), k == [unsafeCGMorphismToArrow morph]] -- special case where i := f >> g >> h
            topObjects c = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
            bindingConeRoles = [BindingCone c | c <- distinguishedCones (target sm), (apex c == target morph) && null (checkNaturalTransformation $ legsCone $ precomposeConeWithMorphism c morph) && (Set.and [init (decompose (tgt funct) (legsCone (precomposeConeWithMorphism c morph) =>$ i)) /= decompose (tgt funct) (legsCone c =>$ i) | i <- topObjects c]) && (Set.all (\m -> and $ (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm)) <$> (filter (/= morph) (decompose (tgt funct) m))) ((Map.values).components.legsCone $ precomposeConeWithMorphism c morph))]
            bottomObjects cc = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArFromWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
            bindingCoconeRoles = [BindingCocone cc | cc <- distinguishedCocones (target sm), (nadir cc == source morph) && null (checkNaturalTransformation $ legsCocone $ postcomposeCoconeWithMorphism cc morph) && (Set.and [ tail (decompose (tgt funct) (legsCocone (postcomposeCoconeWithMorphism cc morph) =>$ i)) /= decompose (tgt funct) (legsCocone cc =>$ i) | i <- bottomObjects cc]) && (Set.all (\m -> and $ (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm)) <$> (filter (/= morph) (decompose (tgt funct) m))) ((Map.values).components.legsCocone $ postcomposeCoconeWithMorphism cc morph))]
            baseTripod t = twoDiagram (universeCategoryTripod t) (internalDomain t) (internalCodomain t)
            bindingTripodRoles = [BindingTripod t (legsCone c =>$ iPow) (legsCone c =>$ iDom) e | t <- distinguishedTripods (target sm), target morph == powerObject t, c <- distinguishedCones (target sm), (cardinal $ ob $ indexingCategoryCone c) == 2 && Set.null (genArrowsWithoutId $ indexingCategoryCone c), iPow <- ob $ indexingCategoryCone c, target (legsCone c =>$ iPow) == (source morph), iDom <- ob $ indexingCategoryCone c, target (legsCone c =>$ iDom) == (internalDomain t), e <- genAr (tgt funct) (apex c) (internalCodomain t), (isMorphismUniquelyDetermined.(getMorphismDeterminacy sm) $ e), morphxid <- genAr (tgt funct) (apex c) (apex $ twoCone t), (powerObjectLeg t) @ morphxid == morph @ (legsCone c =>$ iPow) && (internalDomainLeg t) @ morphxid == (legsCone c =>$ iDom) && e == (evalMap t) @ morphxid]
    
    -- | An object in the target of a sketch morphism can either be uniquely determined, not enough contrained or too much constrained for the sketch morphism to be compilable.
    data ObjectDeterminacy = UniquelyDeterminedObject ObjectRole | NotConstrainedEnoughObject | TooMuchConstrainedObject (Set ObjectRole) | ImageObjectHasAdditionalConstraints (Set ObjectRole) (Set ObjectRole) deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    isObjectUniquelyDetermined :: ObjectDeterminacy -> Bool
    isObjectUniquelyDetermined (UniquelyDeterminedObject _) = True
    isObjectUniquelyDetermined _ = False
    
    -- | Given an object in the target of a sketch morphism, return the determinacy of the object.
    getObjectDeterminacy :: SketchMorphism SketchObject SketchArrow -> SketchObject -> ObjectDeterminacy
    getObjectDeterminacy sm obj
        | Set.null imageRoles && Set.null roles = NotConstrainedEnoughObject
        | Set.null imageRoles && null (tail $ Set.setToList roles) = UniquelyDeterminedObject $ anElement roles
        | Set.null imageRoles = TooMuchConstrainedObject roles
        | not $ null (tail $ Set.setToList imageRoles) = TooMuchConstrainedObject imageRoles
        | cardinal domainRoles == cardinal roles = UniquelyDeterminedObject $ ImageObject antecedent -- if an object is an image, we check if all roles are transported by the sm
        | otherwise = ImageObjectHasAdditionalConstraints domainRoles roles
        where
            funct = underlyingFunctor sm
            roles = getRolesOfObject sm obj
            imageRoles = Set.filter isImageObjectRole roles
            identitySM = identity FinSketch (source sm)
            ImageObject antecedent = anElement imageRoles
            domainRoles = getRolesOfObject identitySM antecedent
    
    -- | A morphism in the target of a sketch morphism can either be uniquely determined, not enough contrained or too much constrained for the sketch morphism to be compilable.
    data MorphismDeterminacy = UniquelyDeterminedMorphism MorphismRole | NotConstrainedEnoughMorphism | TooMuchConstrainedMorphism (Set MorphismRole) | ImageMorphismHasAdditionalConstraints (Set MorphismRole) (Set MorphismRole) deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    isMorphismUniquelyDetermined :: MorphismDeterminacy -> Bool
    isMorphismUniquelyDetermined (UniquelyDeterminedMorphism _) = True
    isMorphismUniquelyDetermined _ = False
    
    -- | Given a morphism in the target of a sketch morphism, return the determinacy of the morphism.
    getMorphismDeterminacy :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> MorphismDeterminacy
    getMorphismDeterminacy sm morph
        | Set.null imageRoles && Set.null roles = NotConstrainedEnoughMorphism
        | Set.null imageRoles && null (tail $ Set.setToList roles) = UniquelyDeterminedMorphism $ anElement roles
        | Set.null imageRoles = TooMuchConstrainedMorphism roles
        | not $ null (tail $ Set.setToList imageRoles) = TooMuchConstrainedMorphism imageRoles
        | cardinal domainRoles == cardinal roles = UniquelyDeterminedMorphism $ ImageMorphism antecedent -- if a morphism is an image, we check if all roles are transported by the sm
        | otherwise = ImageMorphismHasAdditionalConstraints domainRoles roles
        where
            roles = getRolesOfMorphism sm morph
            imageRoles = Set.filter isImageMorphismRole roles
            identitySM = identity FinSketch (source sm)
            ImageMorphism antecedent = anElement imageRoles
            domainRoles = getRolesOfMorphism identitySM antecedent
    
    
    
    
    -- | Remove a law entry in a sketch to check that it was essential to uniquely determine a constituent generator. It returns an "inclusion" sketch morphism even though it is malformed (functoriality is not true).
    removeLawEntryFromSketch :: Sketch SketchObject SketchArrow -> ([Arrow SketchObject SketchArrow],[Arrow SketchObject SketchArrow]) -> SketchMorphism SketchObject SketchArrow
    removeLawEntryFromSketch sketch entry = unsafeSketchMorphism sketch new_sketch funct
        where
            uc = underlyingCategory sketch
            new_cl = Map.delete (fst entry) $ law uc
            new_uc = unsafeCompositionGraph (support uc) new_cl
            funct = completeDiagram Diagram{src = uc, tgt = new_uc, omap = memorizeFunction id (ob uc), mmap = memorizeFunction (\m -> unsafeGetMorphismFromLabel new_uc (unsafeGetLabel m)) (genArrowsWithoutId uc)}
            new_dc = [unsafeCommaObject (funct ->$ (apex c)) One (funct <-@<= (legsCone c)) | c <- distinguishedCones sketch]
            new_dcc = [unsafeCommaObject One (funct ->$ (nadir cc)) (funct <-@<= (legsCocone cc)) | cc <- distinguishedCocones sketch]
            new_t = [unsafeTripod (unsafeCommaObject (funct ->$ (apex (twoCone t))) One (funct <-@<= (legsCone (twoCone t)))) (funct ->£ (evalMap t)) | t <- distinguishedTripods sketch]
            new_sketch = unsafeSketch new_uc new_dc new_dcc new_t
    
    -- | Return wether a law entry is essential for a (co)cone integrity.
    isLawEntryEssentialForAConeIntegrity :: Sketch SketchObject SketchArrow -> ([Arrow SketchObject SketchArrow],[Arrow SketchObject SketchArrow]) -> Bool
    isLawEntryEssentialForAConeIntegrity sk entry = not $ null faulty_cone && null faulty_cocone
        where
            new_sk = target $ removeLawEntryFromSketch sk entry
            faulty_cone = Set.sequenceSet [checkNaturalTransformation $ legsCone c | c <- distinguishedCones new_sk]
            faulty_cocone = Set.sequenceSet [checkNaturalTransformation $ legsCocone cc | cc <- distinguishedCocones new_sk]
    
    -- | Return wether a law entry determines a constituent arrow of the raw paths.
    isLawEntryDeterminingAGenerator :: SketchMorphism SketchObject SketchArrow -> ([Arrow SketchObject SketchArrow],[Arrow SketchObject SketchArrow]) -> Bool
    isLawEntryDeterminingAGenerator skm entry = not.null $ wrongMorphisms
        where
            new_skm = (removeLawEntryFromSketch (target skm) entry) @ skm
            morphismsToExamine = unsafeArrowToCGMorphism (underlyingCategory $ target new_skm) <$> (fst entry ++ snd entry)
            morphismsAndTheirDeterminacy =  [(m,getMorphismDeterminacy new_skm m) | m <- morphismsToExamine] 
            wrongMorphisms = filter (snd.(fmap $ not.isMorphismUniquelyDetermined)) morphismsAndTheirDeterminacy
            
            
    
    -- | Return Just a 'PrecompileError' if the sketch morphism is not compilable, Nothing otherwise.
    isSketchMorphismCompilable :: SketchMorphism SketchObject SketchArrow -> Maybe PrecompileError
    isSketchMorphismCompilable sm
        | not $ Set.null wrongObjects = Just $ WrongObjectDeterminacy wrongObjects
        | not $ Set.null wrongMorphisms = Just $ WrongMorphismDeterminacy wrongMorphisms
        | not $ Set.null entriesNotTransportedBySM = Just $ WrongMorphismDeterminacy entriesNotTransportedBySM
        | not $ Set.null compositionLawEntryNotDeterminingTheirConstituents = Just $ uncurry CompositionLawNotDeterminingAnyOfItsConstituents (anElement compositionLawEntryNotDeterminingTheirConstituents)
        | otherwise = Nothing
        where
            funct = underlyingFunctor sm
            cat = tgt funct
            objectsToExamine = ob cat
            objectsAndTheirDeterminacy =  [(o,getObjectDeterminacy sm o) | o <- objectsToExamine] 
            wrongObjects = Set.filter (snd.(fmap $ not.isObjectUniquelyDetermined)) objectsAndTheirDeterminacy
            morphismsToExamine = genArrowsWithoutId cat
            morphismsAndTheirDeterminacy =  [(m,getMorphismDeterminacy sm m) | m <- morphismsToExamine] 
            wrongMorphisms = Set.filter (snd.(fmap $ not.isMorphismUniquelyDetermined)) morphismsAndTheirDeterminacy
            
            compositionLawEntries = [(entry,entry) | entry <- Map.mapToSet (law cat)]
            compositionLawEntriesWithMorphisms = [(entry,(unsafeArrowToCGMorphism cat <$> rp1, unsafeArrowToCGMorphism cat <$> rp2)) | (entry,(rp1,rp2)) <- compositionLawEntries]
            determinacyLawEntries = [(entry,(getMorphismDeterminacy sm <$> mp1, getMorphismDeterminacy sm <$> mp2)) | (entry,(mp1, mp2)) <- compositionLawEntriesWithMorphisms]
            extractRole (UniquelyDeterminedMorphism r) = r
            extractRole x = error $ "extractRole called on a not uniquely determined morphism :\n"++ pprint 4 x
            roleLawEntires = [(entry, (extractRole <$> dp1, extractRole <$> dp2)) | (entry,(dp1,dp2)) <- determinacyLawEntries]
            onlyImageLawEntries = [(entry,(rp1,rp2)) | (entry,(rp1, rp2)) <- roleLawEntires, all isImageMorphismRole rp1 && all isImageMorphismRole rp2]
            extractImageLaw (ImageMorphism m) = m
            domainMorphismsEntries = [(entry,(extractImageLaw <$> rp1, extractImageLaw <$> rp2)) | (entry,(rp1,rp2)) <- onlyImageLawEntries]
            entriesNotTransportedBySM = [(compose $ unsafeArrowToCGMorphism cat <$> snd entry, ImageMorphismHasAdditionalConstraints (set []) $  getRolesOfMorphism sm (compose $ unsafeArrowToCGMorphism cat <$> snd entry) ||| set [(uncurry CompositionLawEntry entry)]) | (entry,(rp1,rp2)) <- domainMorphismsEntries, compose rp1 /= compose rp2]
            
            compositionLawEntriesWithANonImage = [entry | (entry,(rp1, rp2)) <- roleLawEntires, not $ all isImageMorphismRole rp1 && all isImageMorphismRole rp2]
            compositionLawEntriesNotEssentialForCones = Set.filter (not.(isLawEntryEssentialForAConeIntegrity (target sm))) compositionLawEntriesWithANonImage
            compositionLawEntryNotDeterminingTheirConstituents = Set.filter (not.(isLawEntryDeterminingAGenerator sm)) compositionLawEntriesNotEssentialForCones

    objectToIdentifier :: SketchObject -> T.Text
    objectToIdentifier (Object x) = "object_" <> x
    objectToIdentifier (TerminalObject _) = "terminal"
    objectToIdentifier (InitialObject _) = "initial"
    objectToIdentifier (Product _ xs) = "product_" <> (T.intercalate "_" (objectToIdentifier <$> xs)) <> "_"
    objectToIdentifier (Coproduct _ xs) = "coproduct_" <> (T.intercalate "_" (objectToIdentifier <$> xs)) <> "_"
    objectToIdentifier (Power _ dom codom) = "power_" <> (objectToIdentifier dom) <> "_" <> (objectToIdentifier codom) <> "_"
    
    arrowToIdentifier :: SketchArrow -> T.Text
    arrowToIdentifier (ElementaryArrow x) = "arrow_" <> x
    arrowToIdentifier (Projection obj i) = "proj_" <> (objectToIdentifier obj) <> "_" <> (T.pack $ show i)
    arrowToIdentifier (Coprojection obj i) = "coproj_" <> (objectToIdentifier obj) <> "_" <> (T.pack $ show i)
    arrowToIdentifier (P.EvalMap obj) = "eval_" <> (objectToIdentifier obj)