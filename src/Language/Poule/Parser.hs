{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Parser for poule
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Parser for the Poule programming language.

-}

module Language.Poule.Parser
(
    Parser(..),
    PouleParseError(..),
    -- * Datatypes
    PouleToken(..),
    
    -- ** Types for sketches
    SketchIdentifier(..),
    SketchObject(..),
    parseSketchObject,
    SketchArrow(..),
    parseSketchArrow,
    SketchComposition(..),
    parseSketchComposition,
    SketchStatement(..),
    parseSketchStatement,
    SketchDeclaration(..),
    parseSketchDeclaration,
    
    -- ** Types for sketch morphisms
    SketchMorphismIdentifier(..),
    SketchMorphismStatement(..),
    parseSketchMorphismStatement,
    SketchMorphismDeclaration(..),
    parseSketchMorphismDeclaration,
    
    -- ** Types for models
    ModelIdentifier(..),
    SupportedTargetLanguage(..),
    parseSupportedTargetLanguage,
    ModelDeclaration(..),
    parseModelDeclaration,
    
    -- ** Types for imports
    ImportDeclaration(..),
    parseImportDeclaration,
    
    -- ** Main poule parser 
    ParsedPouleFile(..),
    parsePoule,
)
where

    import              Text.Megaparsec 
    import              Text.Megaparsec.Char
    import qualified    Text.Megaparsec.Char.Lexer as L
    import              Text.Megaparsec.Error.Builder
    import              Control.Monad
    import              Control.Monad.Combinators.Expr
    import              Data.Text
    import              Data.Void
    import              Data.WeakSet
    import              Data.Functor.Identity
    import              Data.Simplifiable
    
    import              GHC.Generics
    
    import              Math.IO.PrettyPrint

    -- * Megaparsec functions

    type Parser = Parsec Void Text

    -- | The type of errors of the Poule parser.
    data PouleParseError = PouleParseError (ParseErrorBundle Text Void) deriving (Eq, Show)
    
    instance PrettyPrint PouleParseError where
        pprint _ (PouleParseError x) = errorBundlePretty x
        pprintWithIndentations _ _ _ (PouleParseError x) = errorBundlePretty x
        pprintIndent  _ _ = ""
        
    instance Simplifiable PouleParseError where
        simplify = id
    
    spaceConsumer :: Parser ()
    spaceConsumer = L.space
        hspace1
        (L.skipLineComment "//")
        (L.skipBlockCommentNested "/*" "*/")
        
    lexeme    = L.lexeme spaceConsumer
    symbol    = L.symbol spaceConsumer

    parens    = between (symbol "(") (symbol ")")
    braces    = between (symbol "{") (symbol "}")
    angles    = between (symbol "<") (symbol ">")
    brackets  = between (symbol "[") (symbol "]")
    delimited p = (try $ parens p) <|> (try $ brackets p)
    semicolon = symbol ";"
    comma     = symbol ","
    colon     = symbol ":"
    dot       = symbol "."
    
    -- | SketchObject symbols
    productSymbol = symbol "*" <|> symbol "&"
    coproductSymbol = symbol "+" <|> symbol "|"
    powerSymbol = try $ symbol "->"
    reversePowerSymbol = symbol "^"
    terminalSymbol = symbol "1"    
    initialSymbol = symbol "0"    
    
    -- | SketchArrow symbols
    projectionSymbol = (try $ symbol "projection") <|> (try $ symbol "proj") <|> (symbol "p")
    coprojectionSymbol = (try $ symbol "coprojection") <|> (try $ symbol "coproj") <|> (symbol "q")
    evalSymbol = (try $ symbol "evaluation") <|> (try $ symbol "eval") <|> (symbol "e")
    
    -- | Composition symbols
    leftToRightCompositionSymbol = (try $ symbol ">>")
    rightToLeftCompositionSymbol = (try $ symbol "<<") <|> dot
    identitySymbol = (try $ symbol "id" <* notFollowedBy (alphaNumChar <|> char '_'))
    
    -- | Composition law entry symbols
    compositionLawSymbol = (try $ symbol "=>") <|> (try $ symbol ":=") <|> symbol "=" <|> symbol "~"
    
    -- | Alias symbols
    aliasSymbol = (try $ symbol "alias")
    
    -- | MultiEqualizer symbols
    multiEqualizerSymbol = (try $ symbol "multiequalizer") <|> (try $ symbol "equalizer") <|> (try $ symbol "eq")
    
    -- | MultiCoequalizer symbols
    multiCoequalizerSymbol = (try $ symbol "multicoequalizer") <|> (try $ symbol "coequalizer") <|> (try $ symbol "coeq")
    
    -- | Instance symbol
    instanceSymbol = (try $ symbol "instance")
    
    -- | Sketch declaration symbol
    sketchDeclarationSymbol = try $ symbol "sketch"
    
    -- | Map object symbol
    mapObjectSymbol = (try $ symbol "->")
    
    -- | Map arrow symbol
    mapArrowSymbol = try $ symbol "=>"
    
    -- | Sketch morphism symbol
    sketchMorphismSymbol = try $ symbol "smorphism" 
    
    -- | Model symbol
    modelSymbol = try $ symbol "model"
    endModelSymbol = try $ symbol "end model"
    
    stringLiteral :: Parser String
    stringLiteral = lexeme $ char '\"' *> manyTill L.charLiteral (char '\"')
    
    identifier :: Parser Text
    identifier = lexeme $ do
        x <- letterChar <|> char '_'
        xs <- many (alphaNumChar <|> char '_')
        return $ pack $ x:xs
        
    statementSeparator :: Parser ()
    statementSeparator = () <$ (some $ lexeme $ choice [() <$ eol,() <$ char ';'])
    
    -- | @'sepBy2' p sep@ parses /two/ or more occurrences of @p@, separated by
    -- @sep@. Returns a list of values returned by @p@.
    sepBy2 :: MonadPlus m => m a -> m sep -> m [a]
    sepBy2 p sep = do
      x <- p
      sep
      y <- p
      ([x,y] ++) <$> many (sep >> p)

    -- * Poule types

    -- | A token for the Poule programming language
    data PouleToken =  Import ImportDeclaration
                     | Sketch SketchDeclaration
                     | SketchMorphism SketchMorphismDeclaration
                     | Model ModelDeclaration
                     deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)


    -- ** Types of parsed statements    
    
    -- ** Types for sketches
    
    -- | A sketch identifier is a text.
    type SketchIdentifier = Text
    
    type Aliases = Set Text
    
    -- | An object is either a text, the apex of a product cone, the nadir of a coproduct cocone or a power object. Each object contains a set of aliases.
    data SketchObject = Object Text
                      | TerminalObject Aliases
                      | InitialObject Aliases
                      | Product Aliases [SketchObject]
                      | Coproduct Aliases [SketchObject]
                      | Power Aliases SketchObject SketchObject
                      deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    data BannedOperators = Coprod | Prod | Pow deriving (Eq, Show)        
    
    parseObj :: Parser SketchObject
    parseObj = Object <$> identifier

    parseProduct :: Set BannedOperators -> Parser SketchObject
    parseProduct bannedOperators = do
        objs <- sepBy2 (parseSketchObjectHelper (insert Prod bannedOperators)) productSymbol
        return $ Product (set []) objs
    
    parseCoproduct :: Set BannedOperators -> Parser SketchObject
    parseCoproduct bannedOperators = do
        objs <- sepBy2 (parseSketchObjectHelper (insert Coprod bannedOperators)) coproductSymbol
        return $ Coproduct (set []) objs
    
    parseTerminalObject :: Parser SketchObject
    parseTerminalObject = TerminalObject (set []) <$ terminalSymbol
    
    parseInitialObject :: Parser SketchObject
    parseInitialObject = InitialObject (set []) <$ initialSymbol
    
    parseArrowPower :: Set BannedOperators -> Parser SketchObject
    parseArrowPower bannedOperators = do
        a <- parseSketchObjectHelper (insert Pow bannedOperators)
        powerSymbol
        b <- parseSketchObjectHelper bannedOperators
        return $ Power (set []) a b
        
    parseReversePower :: Set BannedOperators -> Parser SketchObject
    parseReversePower bannedOperators = do
        a <- parseSketchObjectHelper (insert Pow bannedOperators)
        reversePowerSymbol
        b <- parseSketchObjectHelper bannedOperators
        return $ Power (set []) b a
        
    parsePower :: Set BannedOperators -> Parser SketchObject
    parsePower bannedOperators = try (parseArrowPower bannedOperators) <|> (parseReversePower bannedOperators)
    
    parseSketchObjectHelper :: Set BannedOperators -> Parser SketchObject
    parseSketchObjectHelper bannedOperators = choice $ (operatorToParser <$> remainingOperators) ++ defaultParsers
        where
            defaultParsers = [try $ delimited $ parseCoproduct (set []), try $ delimited $ parseProduct (set []), try $ delimited $ parsePower (set []), parseTerminalObject, parseInitialObject, parseObj]
            operatorToParser Prod = try $ parseProduct bannedOperators
            operatorToParser Coprod = try $ parseCoproduct bannedOperators
            operatorToParser Pow = try $ parsePower bannedOperators
            remainingOperators = setToList $ set [Coprod, Prod, Pow] |-| bannedOperators
    
    -- | Parser for a 'SketchObject'.
    parseSketchObject :: Parser SketchObject
    parseSketchObject = parseSketchObjectHelper (set [])
    
    -- | An arrow identifier is either an elementary arrow name as a Text, a projection arrow, a coprojection arrow or an evaluation map.
    data SketchArrow = ElementaryArrow Text
                     | Projection SketchObject Int
                     | Coprojection SketchObject Int
                     | EvalMap SketchObject 
                     deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
                     
    parseElementaryArrow :: Parser SketchArrow
    parseElementaryArrow = ElementaryArrow <$> identifier
    
    parseProjection :: Parser SketchArrow
    parseProjection = do
        projectionSymbol
        delimited $ do
            obj <- parseSketchObject
            comma
            i <- L.decimal
            return $ Projection obj i
            
    parseCoprojection :: Parser SketchArrow
    parseCoprojection = do
        coprojectionSymbol
        delimited $ do
            obj <- parseSketchObject
            comma
            i <- L.decimal
            return $ Coprojection obj i
            
    parseEval :: Parser SketchArrow
    parseEval = do
        evalSymbol
        delimited $ do
            obj <- parseSketchObject
            return $ EvalMap obj
    
    
    -- | Parser for an arrow in a sketch.
    parseSketchArrow :: Parser SketchArrow
    parseSketchArrow = try parseCoprojection <|> try parseProjection <|> try parseEval <|> parseElementaryArrow
    
    
    -- | A sketch composition is a composition of sketch arrows.
    type SketchComposition = [SketchArrow]
    
    parseIdentity :: Parser SketchComposition
    parseIdentity = [] <$ identitySymbol
    
    parseLeftToRightComposition :: Parser SketchComposition
    parseLeftToRightComposition = sepBy2 parseSketchArrow leftToRightCompositionSymbol 
    
    parseRightToLeftComposition :: Parser SketchComposition    
    parseRightToLeftComposition = Prelude.reverse <$> sepBy1 parseSketchArrow rightToLeftCompositionSymbol
        
    -- | Parser for a composition of sketch arrows.
    parseSketchComposition :: Parser SketchComposition
    parseSketchComposition = (try parseIdentity) <|> (try parseLeftToRightComposition) <|> parseRightToLeftComposition
    
    -- | A sketch statement either declares a new object, a new arrow, a composition law entry, a (co)product alias, a (co)equalizer definition, a pullback/pushforward definition or an instance.
    data SketchStatement = NewObject SketchObject 
                         | NewArrow SketchArrow SketchObject SketchObject
                         | LawEntry SketchComposition SketchComposition
                         | TypeAlias Text SketchObject
                         | MultiEqualizer SketchObject SketchObject [(SketchComposition,SketchComposition)]
                         | MultiCoequalizer SketchObject SketchObject [(SketchComposition,SketchComposition)]
                         | Instance SketchMorphismIdentifier [SketchMorphismStatement]
                         | InstanceInclusion SketchMorphismIdentifier
                         deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    
    -- | A sketch declaration is a sketch created from scratch thanks to a list of 'SketchStatement's.
    data SketchDeclaration = SketchDeclaration SketchIdentifier [SketchStatement] deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    parseNewObject :: Parser SketchStatement
    parseNewObject = do
        newObj <- parseSketchObject
        if newObj == (Object "instance") then mzero
        else do
                notFollowedBy aliasSymbol
                return $ NewObject newObj
    
    isPower :: SketchObject -> Bool
    isPower (Power _ _ _) = True
    isPower _ = False
    
    parseNewArrow :: Parser SketchStatement
    parseNewArrow = do
        name <- parseElementaryArrow
        symbol ":"
        body <- parseSketchObject
        if isPower body then do
            let (Power _ dom codom) = body
            return $ NewArrow name dom codom
        else do
            fail $ "Expected a morphism declaration.\n Parsed type is "++ show body ++"\nCheck the parentheses around the domain and the codomain of the arrow."
    
    parseLawEntry :: Parser SketchStatement
    parseLawEntry = do
        left <- parseSketchComposition
        compositionLawSymbol
        right <- parseSketchComposition
        return $ LawEntry left right
    
    isAtomicObject :: SketchObject -> Bool
    isAtomicObject (Object _) = True
    isAtomicObject _ = False
    
    
    
    parseTypeAlias :: Parser SketchStatement
    parseTypeAlias = do
        (Object alias) <- parseObj
        aliasSymbol
        original <- parseSketchObject
        if isAtomicObject original then fail "The left hand side of an alias statement must be a product, a coproduct or an exponential object." else do
            return $ TypeAlias alias original
    
    parseEq :: Parser (SketchComposition, SketchComposition)
    parseEq = do
        left <- parseSketchComposition
        comma
        right <- parseSketchComposition
        return $ (left,right)
    
    parseMultiEqualizer :: Parser SketchStatement
    parseMultiEqualizer = do
        newObj <- parseObj
        multiEqualizerSymbol
        prevObj <- parseSketchObject
        eqs <- some (delimited parseEq)
        return $ MultiEqualizer newObj prevObj eqs
        
    parseMultiCoequalizer :: Parser SketchStatement
    parseMultiCoequalizer = do
        newObj <- parseObj
        multiCoequalizerSymbol
        prevObj <- parseSketchObject
        eqs <- some (delimited parseEq)
        return $ MultiCoequalizer newObj prevObj eqs
    
    parseInstance :: Parser SketchStatement
    parseInstance = do            
        instanceSymbol
        class_ <- identifier
        many statementSeparator
        smstmts <- braces parseSketchMorphismBody
        return $ Instance class_ smstmts
        
    parseInstanceInclusion :: Parser SketchStatement
    parseInstanceInclusion = do            
        instanceSymbol
        class_ <- identifier
        symbol "inclusion"
        return $ InstanceInclusion class_
       
    -- | Parser for a sketch statement.
    parseSketchStatement :: Parser SketchStatement
    parseSketchStatement = (choice $ [try parseInstanceInclusion, try parseInstance, try parseMultiCoequalizer, try parseMultiEqualizer, try parseTypeAlias, try parseLawEntry, try parseNewArrow, parseNewObject <* notFollowedBy (choice [symbol ":", compositionLawSymbol, multiEqualizerSymbol, multiCoequalizerSymbol])]) 
    
    parseSketchBody :: Parser [SketchStatement]
    parseSketchBody = do
        many statementSeparator
        many (parseSketchStatement <* many statementSeparator)
    
    -- | Parser for a sketch declaration.
    parseSketchDeclaration :: Parser PouleToken
    parseSketchDeclaration = do
        sketchDeclarationSymbol
        name <- identifier
        many statementSeparator
        stmts <- braces parseSketchBody
        return $ Sketch $ SketchDeclaration name stmts
    
    -- ** Types for sketch morphisms
    
    -- | A sketch morphism identifier is a text.
    type SketchMorphismIdentifier = Text
    
    -- | A sketch morphism statement is a mapping of object or a mapping of arrow to a morphism.
    data SketchMorphismStatement = MapObject SketchObject SketchObject 
                                 | MapArrow SketchArrow SketchComposition 
                                 deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    parseMapObject :: Parser SketchMorphismStatement
    parseMapObject = do
        left <- parseObj
        mapObjectSymbol
        right <- parseSketchObject
        return $ MapObject left right
        
    parseMapArrow :: Parser SketchMorphismStatement
    parseMapArrow = do
        left <- parseElementaryArrow
        mapArrowSymbol
        right <- parseSketchComposition
        return $ MapArrow left right
    
    -- | Parser for a sketch morphism statement.
    parseSketchMorphismStatement :: Parser SketchMorphismStatement
    parseSketchMorphismStatement = (try parseMapArrow) <|> (try parseMapObject)
    
    parseSketchMorphismBody :: Parser [SketchMorphismStatement]
    parseSketchMorphismBody = do
        many statementSeparator
        result <- many (parseSketchMorphismStatement <* many statementSeparator)
        many statementSeparator
        return result
    
    -- | A sketch morphism declaration is either a sketch morphism created from scratch or an inclusion sketch morphism.
    data SketchMorphismDeclaration = SketchMorphismDeclaration SketchMorphismIdentifier SketchIdentifier SketchIdentifier [SketchMorphismStatement]
                                   | SketchMorphismInclusion SketchMorphismIdentifier SketchIdentifier SketchIdentifier
                                   deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)

    parseSketchMorphismArguments :: Parser (SketchIdentifier, SketchIdentifier)
    parseSketchMorphismArguments = do
        left <- identifier
        comma
        right <- identifier
        return $ (left,right)
    
    parseSketchMorphismInclusion :: Parser SketchMorphismDeclaration
    parseSketchMorphismInclusion = do
        sketchMorphismSymbol
        name <- identifier
        symbol "inclusion"
        (dom,codom) <- delimited parseSketchMorphismArguments
        return $ SketchMorphismInclusion name dom codom
        
    parseSketchMorphismDecl :: Parser SketchMorphismDeclaration
    parseSketchMorphismDecl = do
        sketchMorphismSymbol
        name <- identifier
        (dom,codom) <- delimited parseSketchMorphismArguments
        many statementSeparator
        stmts <- braces parseSketchMorphismBody
        return $ SketchMorphismDeclaration name dom codom stmts
        
    -- | Parser for a sketch morphism declaration.
    parseSketchMorphismDeclaration :: Parser PouleToken
    parseSketchMorphismDeclaration = SketchMorphism <$> (try parseSketchMorphismInclusion <|> parseSketchMorphismDecl)

    -- ** Types for models.
    
    -- | A model identifier is a text.
    type ModelIdentifier = Text
    
    -- | A supported target language for the transcompilation.
    data SupportedTargetLanguage = Haskell | Python | C deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    -- | Parser for a support target language.
    parseSupportedTargetLanguage :: Parser SupportedTargetLanguage
    parseSupportedTargetLanguage = (try $ Haskell <$ symbol "Haskell") <|> (try $ Python <$ symbol "Python") <|> (try $ C <$ symbol "C")
    
    -- | A model declaration contains foreign code and may instantiate other models.
    data ModelDeclaration = ModelDeclaration ModelIdentifier SketchIdentifier SupportedTargetLanguage [ModelIdentifier] Text deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)
    
    parseModelArguments :: Parser (SketchIdentifier,SupportedTargetLanguage)
    parseModelArguments = do
        sk <- identifier
        comma
        lang <- parseSupportedTargetLanguage
        return (sk,lang)
        
    -- | Parser for a model declaration.
    parseModelDeclaration :: Parser PouleToken
    parseModelDeclaration = do
        modelSymbol
        name <- identifier
        (sk,lang) <- delimited parseModelArguments
        inst <- option [] (do
            instanceSymbol
            many identifier)
        foreign_code <- pack <$> manyTill anySingle endModelSymbol
        return $ Model $ ModelDeclaration name sk lang inst foreign_code
    
    
    -- ** Types for imports
    
    -- | An imported or exported object.
    data ImportDeclaration = FromImportAsStatement FilePath Text Text
                           | FromExportAsStatement FilePath Text Text
                           | ImportHidingStatement FilePath [Text]
                           | ExportHidingStatement FilePath [Text]
                           deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)

    parseFromImportAsStatement :: Parser ImportDeclaration
    parseFromImportAsStatement = do
        symbol "from"
        filename <- stringLiteral
        symbol "import"
        idImported <- identifier
        newId <- option idImported (do
            symbol "as"
            identifier)
        return $ FromImportAsStatement filename idImported newId
        
    parseImportHidingStatement :: Parser ImportDeclaration
    parseImportHidingStatement = do
        symbol "import"
        filename <- stringLiteral
        hidingIds <- option [] (do
            symbol "hiding"
            delimited $ sepBy identifier comma
            )
        return $ ImportHidingStatement filename hidingIds
    
    parseFromExportAsStatement :: Parser ImportDeclaration
    parseFromExportAsStatement = do
        symbol "from"
        filename <- stringLiteral
        symbol "export"
        idExported <- identifier
        newId <- option idExported (do
            symbol "as"
            identifier)
        return $ FromExportAsStatement filename idExported newId
        
    parseExportHidingStatement :: Parser ImportDeclaration
    parseExportHidingStatement = do
        symbol "export"
        filename <- stringLiteral
        hidingIds <- option [] (do
            symbol "hiding"
            delimited $ sepBy identifier comma
            )
        return $ ExportHidingStatement filename hidingIds
       
        
    -- | Parser of import declaration.
    parseImportDeclaration :: Parser PouleToken
    parseImportDeclaration = Import <$> ((try parseFromImportAsStatement) <|> (try parseImportHidingStatement) <|> (try parseFromExportAsStatement) <|> parseExportHidingStatement)
    

    -- ** Poule main parser
    
    -- | A list of parsed Poule tokens read from a file.
    type ParsedPouleFile = [PouleToken]

    -- | Main poule parser.
    parsePoule :: Parser ParsedPouleFile
    parsePoule = do
        spaceConsumer
        many statementSeparator
        result <- sepBy (choice $ try <$> [parseImportDeclaration, parseSketchMorphismDeclaration, parseModelDeclaration, parseSketchDeclaration, undefined <$ eof]) (some statementSeparator)
        if Prelude.null result then fail "No line could be parsed." else do
            return $ Prelude.init result