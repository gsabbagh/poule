{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Compiler for Poule.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Compiler for Poule.

-}

module Language.Poule.Compiler
(
    CompileError(..),
    CompileOptions(..),
    parseOptionsCompiler,
    parseOptionsCompilerWithInfo,
    compile,
)

where
    import              Language.Poule.Parser       hiding (Parser)
    import              Language.Poule.Preprocesser
    import              Language.Poule.SemanticAnalyser
    import              Language.Poule.Precompiler
    import              Language.Poule.Transcompilers
    
    import              Math.IO.PrettyPrint
    import              Math.Category
    import              Math.Categories.FinSketch hiding (sketchMorphism)
    import              Math.FiniteCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch           hiding(sketchMorphism)

    import qualified    Data.Text               as   T
    import qualified    Data.Text.IO            as   TEXTIO
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    import              Options.Applicative
    
    import              System.Directory                            (createDirectoryIfMissing)
    import              System.FilePath.Posix
    import              System.Process                              (callCommand)
    
    import              Data.Version (showVersion)
    import              Paths_poule (version)

    
    data CompilerError = UnknownModel T.Text
                       | UnknownSketchMorphism T.Text
                       | ModelAndSketchMorphismHaveDifferentSource T.Text T.Text 
                       | IdentifierIsNotModel T.Text 
                       | IdentifierIsNotSketchMorphism T.Text 
                       | UnknownMainFunction T.Text 
                       deriving (Show, Eq, Generic, PrettyPrint, Simplifiable)
    
    data CompileError = PreprocesserError PreprocessError 
                      | SemanticAnalyserError SemanticError 
                      | PrecompilerError PrecompileError
                      | CompilerError CompilerError
        deriving (Show, Eq, Generic, PrettyPrint, Simplifiable)
    
    data CompileOptions = CompileOptions {
        inputFile  :: FilePath,
        sketchMorphism :: String,
        model :: String,
        outputFile :: FilePath,
        auxiliaryDirectory :: String,
        transcompileOnly :: Bool,
        analyseSemanticallyOnly :: Bool,
        mainIs :: String,
        verbosity :: Int,
        compilerExecutable :: FilePath,
        additionalOptions :: String
        } deriving (Eq, Show)
    
    parseOptionsCompiler :: Parser CompileOptions
    parseOptionsCompiler = CompileOptions
        <$> argument str
            (  metavar "INPUT_FILE"
            <> help "Input file for the compilation process." )
        <*> argument str
            (  metavar "SKETCH_MORPHISM_IDENTIFIER"
            <> help "Sketch morphism to compile.")
        <*> argument str
            (  help "The identifier of the model to enrich. The type of the model determines the intermediate language Poule will use."
            <> metavar "MODEL_IDENTIFIER" )
        <*> option str
            (  long "out"
            <> short 'o'
            <> help "The file path of the output executable file."
            <> showDefault
            <> value ""
            <> metavar "FILE_PATH" )
        <*> option str
            (  long "auxiliary"
            <> short 'a'
            <> help "The path of the auxiliary directory where intermediate files are written."
            <> showDefault
            <> value "auxiliary"
            <> metavar "DIR" )
        <*> switch
            (  short 't'
            <> long "transcompile-only"
            <> help "Transcompile only; do not compile the resulting file using another compiler.")
        <*> switch
            (  short 's'
            <> long "semantic-only"
            <> help "Only perform the semantic analysis; do not transcompile.")
        <*> option str
            (  long "main-is"
            <> short 'm'
            <> help "Identifier of the main morphism."
            <> showDefault
            <> value "main"
            <> metavar "STR" )
        <*> option auto
            (  long "verbosity"
            <> short 'v'
            <> help "Verbosity of the compilation process."
            <> showDefault
            <> value 1
            <> metavar "INT" )
        <*> option str
            (  long "compiler"
            <> short 'c'
            <> help "Path to the executable of the compiler of the intermediate language."
            <> showDefault
            <> value ""
            <> metavar "FILE_PATH")
        <*> option str
            (  long "options"
            <> help "Additional options to pass to the intermediate compiler."
            <> showDefault
            <> value ""
            <> metavar "STR" )
        
         
    versioner :: Parser (a -> a)
    versioner = infoOption (showVersion version) (long "version" <> help "Show version" <> hidden)
    
    parseOptionsCompilerWithInfo :: ParserInfo CompileOptions
    parseOptionsCompilerWithInfo = info (parseOptionsCompiler <**> helper <**> versioner)( fullDesc
                                                                         <> progDesc "Compile a Poule program file. The compilation of a model by a sketch morphism is the inverse precomposition functor associated to the sketch morphism applied to the model."
                                                                         <> header "Poule compiler" )
                                                                         
    
    createAndWriteFile :: FilePath -> T.Text -> IO ()
    createAndWriteFile path content = do
        createDirectoryIfMissing True $ takeDirectory path
        TEXTIO.writeFile path content
    
    isModel (SemanticModel _ _) = True
    isModel _ = False
    
    isSM (SemanticSketchMorphism _) = True
    isSM _ = False
    
    compile :: CompileOptions -> IO (Maybe CompileError)
    compile options = do
        errOrPreprocessedFile <- preprocessPouleFile (inputFile options)
        if null errOrPreprocessedFile then do
            let Left err = errOrPreprocessedFile
            return $ Just $ PreprocesserError err
        else do
            putStrLn "File parsed successfully"
            let Right preprocessedProgram = errOrPreprocessedFile
            if analyseSemanticallyOnly options then do
                let errOrSemanticProgram = analyseSemantically preprocessedProgram
                if null errOrSemanticProgram then do
                    let Left err = errOrSemanticProgram
                    return $ Just $ SemanticAnalyserError err
                else do
                    let Right semanticProgram = errOrSemanticProgram
                    putStrLn $ show $ semanticProgram
                    return Nothing
            else do
                let identifierSketchMorphism = T.pack $ sketchMorphism options
                if null $ preprocessedProgram |?| (inputFile options, identifierSketchMorphism) then return $ Just $ CompilerError $ UnknownSketchMorphism $ (T.pack $ sketchMorphism options)
                else do
                    let errOrSemanticProgram = analyseSemantically preprocessedProgram
                    if null errOrSemanticProgram then do
                        let Left err = errOrSemanticProgram
                        return $ Just $ SemanticAnalyserError err
                    else do
                        putStrLn "Semantic analysis successful"
                        let Right semProg = errOrSemanticProgram
                        let SemanticSketchMorphism bla = semProg |!| (inputFile options, identifierSketchMorphism)
                        let maybeSmorphism = semProg |?| (inputFile options, identifierSketchMorphism)
                        if null maybeSmorphism then return $ Just $ CompilerError $ UnknownSketchMorphism identifierSketchMorphism
                        else do
                            if not $ isSM (semProg |!| (inputFile options,identifierSketchMorphism)) then return $ Just $ CompilerError $ IdentifierIsNotSketchMorphism identifierSketchMorphism else do
                                let (Just (SemanticSketchMorphism sm)) = semProg |?| (inputFile options,identifierSketchMorphism)
                                let maybeCompilationError = isSketchMorphismCompilable sm
                                if not $ null maybeCompilationError then do
                                    let (Just err) = maybeCompilationError
                                    return $ Just $ PrecompilerError err
                                else do 
                                    putStrLn $ "Sketch morphism is compilable"
                                    let identifierModel = T.pack $ model options
                                    let maybeModel = semProg |?| (inputFile options, identifierModel)
                                    if null maybeModel then return $ Just $ CompilerError $ UnknownModel identifierModel
                                    else do
                                        let (Just semanticM) = semProg |?| (inputFile options,identifierModel)
                                        if not $ isModel semanticM then return $ Just $ CompilerError $ IdentifierIsNotModel identifierModel else do
                                            let (SemanticModel lang foreignCode) = semanticM
                                            putStrLn $ show $ semProg |?| (inputFile options,identifierModel)
                                            case lang of
                                                Haskell -> do
                                                    let typesCode = transformObjectToHaskellCode sm <$> ob (underlyingCategory $ target sm)
                                                    let functionsCode = transformMorphismToHaskellCode sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    let mainCode = "main = "<>haskellFunctionPrefix <> "arrow_" <> (T.pack (mainIs options)) <> " undefined;\n"
                                                    let code =  "-- Haskell model\n\n" <> foreignCode <> "\n\n-- Poule generated code\n\ndata Void\n"<> T.concat (setToList typesCode) <> "\n" <> T.concat (setToList functionsCode) <> "\n\n-- main function\n\n" <> mainCode
                                                    let fileName = ((auxiliaryDirectory options) </> (takeBaseName (inputFile options)) <.> ".hs")
                                                    createAndWriteFile fileName code
                                                    if transcompileOnly options then return Nothing
                                                    else do
                                                        let executableName = (if outputFile options == "" then (takeBaseName (inputFile options)) <.> ".exe" else outputFile options)
                                                        callCommand $ (if compilerExecutable options == "" then "ghc" else compilerExecutable options)++ " -o " ++ executableName ++ " " ++ (additionalOptions options) ++ " " ++ fileName
                                                        return Nothing
                                                Python -> do
                                                    let functionsCode = transformMorphismToPythonCode sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    let mainCode = "if __name__ == \"__main__\":\n" <> pythonIndent 1 <> pythonFunctionPrefix <> "arrow_" <> (T.pack $ mainIs options) <> "({})\n"
                                                    let code =  "# Python model\n\n" <> foreignCode <> "\n\n# Poule generated code\n\n"<> T.concat (setToList functionsCode) <> "\n\n# main function\n\n" <> mainCode
                                                    let executableName = (if outputFile options == "" then (takeBaseName (inputFile options)) <.> ".py" else outputFile options)
                                                    createAndWriteFile executableName code
                                                    return Nothing
                                                C -> do
                                                    let forwardTypeDeclaration = forwardDefineType sm <$> ob (underlyingCategory $ target sm)
                                                    let forwardFunctionDeclaration = forwardDefineFunction sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    let typesCode = transformObjectToCCode sm <$> ob (underlyingCategory $ target sm)
                                                    putStrLn $ Set.concat (T.unpack <$> typesCode)
                                                    let functionsCode = transformMorphismToCCode sm <$> genArrowsWithoutId (underlyingCategory $ target sm)
                                                    putStrLn $ Set.concat (T.unpack <$> functionsCode)
                                                    let mainName = T.pack $ mainIs options
                                                    let maybeLabel = getMorphismFromLabel (underlyingCategory $ target sm) (ElementaryArrow mainName)
                                                    if null maybeLabel then return $ Just $ CompilerError $ UnknownMainFunction $ mainName else do
                                                        let mainCode = "void main(){\n" <> cIndent 1 <> objectToCIdentifier sm (source $ unsafeGetMorphismFromLabel (underlyingCategory $ target sm) (ElementaryArrow mainName)) <> " x = {};\n" <> cIndent 1 <> morphismToCIdentifier sm (unsafeGetMorphismFromLabel (underlyingCategory $ target sm) (ElementaryArrow mainName)) <> "(&x);\n}\n"
                                                        let code =  "// C model\n\n" <> "#include <stdio.h>\n#include <stdlib.h>\n\ntypedef struct Term{} Term;" <> foreignCode <> "\n\n// Poule generated code\n\n"<> T.concat (setToList forwardTypeDeclaration) <> T.concat (setToList typesCode) <> "\n" <> T.concat (setToList forwardFunctionDeclaration) <> T.concat (setToList functionsCode) <> "\n\n// main function\n\n" <> mainCode
                                                        let fileName = ((auxiliaryDirectory options) </> (takeBaseName (inputFile options)) <.> ".c")
                                                        createAndWriteFile fileName code
                                                        if transcompileOnly options then return Nothing
                                                        else do
                                                            let executableName = (if outputFile options == "" then (takeBaseName (inputFile options)) <.> ".exe" else outputFile options)
                                                            callCommand $ (if compilerExecutable options == "" then "gcc" else compilerExecutable options)++ " -o " ++ executableName ++ " " ++ (additionalOptions options) ++ " " ++ fileName
                                                            return Nothing
