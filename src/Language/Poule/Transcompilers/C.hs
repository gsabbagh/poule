{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Transcompiler from Poule to C.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transcompiler from Poule to C.

-}

module Language.Poule.Transcompilers.C
(
    cIndent,
    cTypePrefix,
    cVarPrefix,
    cFunctionPrefix,
    objectToCIdentifier,
    morphismToCIdentifier,
    forwardDefineType,
    forwardDefineFunction,
    transformObjectToCCode,
    transformMorphismToCCode,
)

where
    import              Language.Poule.Parser  as P  hiding (Arrow)
    import              Language.Poule.SemanticAnalyser
    import              Language.Poule.Precompiler
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint
    
    import              Data.List                   (elemIndex)
    import qualified    Data.Text               as  T
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    cIndent :: Int -> T.Text
    cIndent 0 = ""
    cIndent x = "    "<>(cIndent $ x-1)
    
    cTypePrefix :: T.Text
    cTypePrefix = "Poule_"
    
    cVarPrefix :: T.Text
    cVarPrefix = "var_"
    
    -- | Transform an object of the target sketch into a C type identifier.
    objectToCIdentifier :: SketchMorphism SketchObject SketchArrow -> SketchObject -> T.Text
    objectToCIdentifier sm obj = case role of
        (ImageObject (Object x)) -> x
        (ImageObject (TerminalObject _)) -> "Term"
        (ImageObject (InitialObject _)) -> "void"
        (ImageObject x@(Product aliases _))
            | Set.null aliases -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            | otherwise -> anElement aliases
        (ImageObject x@(Coproduct aliases _))
            | Set.null aliases -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            | otherwise -> anElement aliases
        (ImageObject x@(Power aliases _ _))
            | Set.null aliases -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            | otherwise -> anElement aliases
        (ApexObject x) -> cTypePrefix <> objectToIdentifier (apex x)
        (NadirObject x) -> cTypePrefix <> objectToIdentifier (nadir x)
        (PowerObject t) -> cTypePrefix <> objectToIdentifier (powerObject t)
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
    
    
    -- | Transforms an object of the target of a sketch morphism into C code.
    transformObjectToCCode :: SketchMorphism SketchObject SketchArrow -> SketchObject -> T.Text
    transformObjectToCCode sm obj = case role of
        (ImageObject _) -> ";\n"
        (ApexObject c) -> newApex
            where
                b = baseCone c
                topObjects = [i | i <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) i), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
                newApex = "struct "<>objectToCIdentifier sm obj<>"{\n" <> (T.concat $ setToList [cIndent 1 <> (objectToCIdentifier sm (baseCone c ->$ x)) <> "* " <> cVarPrefix <> (morphismToCIdentifier sm (legsCone c =>$ x)) <> ";\n" | x <- topObjects])<>"};\n\n"
        (NadirObject cc) -> newNadir
            where
                b = baseCocone cc
                bottomObjects = [i | i <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArFromWithoutId (indexingCategoryCocone cc) i), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
                newNadir = "struct " <> objectToCIdentifier sm obj <> "{\n" <> (T.concat $ setToList [cIndent 1 <> (objectToCIdentifier sm (baseCocone cc ->$ x)) <> "* " <> cVarPrefix <> (morphismToCIdentifier sm (legsCocone cc =>$ x)) <> ";\n" | x <- bottomObjects])<>"\n"<> cIndent 1 <> "int current_type;\n};\n\n"
        (PowerObject t) -> "struct "<>objectToCIdentifier sm obj<>"{\n" <> cIndent 1 <> objectToCIdentifier sm (internalCodomain t) <> "* (*uncurried_function_pointer) (void*);\n" <> cIndent 1 <>  "void* (*pair_constructor)(void*," <> objectToCIdentifier sm (internalDomain t)<>"*);\n"<>cIndent 1<>"void* first_arg;\n};\n\n"
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
            
            
    forwardDefineType :: SketchMorphism SketchObject SketchArrow -> SketchObject -> T.Text
    forwardDefineType sm obj = case role of
        (ImageObject x) -> ""
        (ApexObject x) -> "typedef struct " <> objectToCIdentifier sm obj <> " " <> objectToCIdentifier sm obj <> ";\n"
        (NadirObject x) -> "typedef struct " <> objectToCIdentifier sm obj <> " " <> objectToCIdentifier sm obj <> ";\n"
        (PowerObject t) -> "typedef struct " <> objectToCIdentifier sm obj <> " " <> objectToCIdentifier sm obj <> ";\n"
        where
            role = (anElement $ getRolesOfObject sm obj)
            
    cFunctionPrefix :: T.Text
    cFunctionPrefix = "poule_"
         
    -- | Transform a non identity generating morphism of the target of a sketch morphism into a C identifier.
    morphismToCIdentifier :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    morphismToCIdentifier sm morph
        | isComposite (underlyingCategory $ target sm) morph = error $ "morphismToCIdentifier called on a composite morphism : "++ (show morph)
        | otherwise = case role of
            (ImageMorphism x) -> case unsafeGetLabel x of
                (ElementaryArrow y) -> y
                (Projection obj i) -> "projection_" <> objectToCIdentifier sm obj <> "_" <> (T.pack $ show i)
                (Coprojection obj i) -> "coprojection_" <> objectToCIdentifier sm obj <> "_" <> (T.pack $ show i)
                (EvalMap obj) -> "eval_" <> objectToCIdentifier sm obj
            (ConeLeg c i) -> cFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (CoconeLeg cc i) -> cFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (EvaluationMap t) -> cFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (IdentityMorphism m) -> error $ "identity morphism given to morphismToCIdentifier : "++ show morph
            (CompositeMorphism decomp) -> cFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingCone c) -> cFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingCocone cc) -> cFunctionPrefix <> arrowToIdentifier(unsafeGetLabel morph)
            (BindingTripod t powLeg domLeg codomLeg) -> cFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in morphismToCIdentifier"
            where
                UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
            
    forwardDefineFunction :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    forwardDefineFunction sm morph = case role of
        (ImageMorphism _) -> ";\n"
        (ConeLeg c i) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (CoconeLeg cc i) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (EvaluationMap t) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (IdentityMorphism m) -> ";\n"
        (CompositeMorphism decomp) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (BindingCone c) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (BindingCocone cc) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (BindingTripod t powLeg domLeg codomLeg) -> objectToCIdentifier sm (source powLeg) <> "* pair_constructor_" <> morphismToCIdentifier sm morph <> "(" <> objectToCIdentifier sm (target powLeg) <> "* x, " <> objectToCIdentifier sm (target domLeg) <> "* y);\n" <> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x);\n"
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in forwardDefineFunction"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
          
    -- | Transforms a generating morphism of the target of a sketch morphism into C code.
    transformMorphismToCCode :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    transformMorphismToCCode sm morph = case role of
        (ImageMorphism _) -> ";\n"
        (ConeLeg c i) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x){ return x->" <> cVarPrefix <> morphismToCIdentifier sm morph <> ";}\n\n"
        (CoconeLeg cc i) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x){\n"<> cIndent 1 <> objectToCIdentifier sm (target morph) <> "* y = malloc(sizeof(" <> objectToCIdentifier sm (target morph) <> "));\n" <> cIndent 1 <> "y->" <> cVarPrefix <> morphismToCIdentifier sm morph <> " = x;\n" <> cIndent 1 <> "y->current_type = " <> (T.pack $ show $ fromJust $ elemIndex i $ Set.setToList $ ob $ indexingCategoryCocone cc) <> ";\n" <> cIndent 1 <> "return y;\n}\n\n"
            where 
                fromJust (Just x) = x
        (EvaluationMap t) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x){\n"<> cIndent 1 <> objectToCIdentifier sm (target morph) <>"* r = malloc(sizeof(" <> objectToCIdentifier sm (target morph) <> "));\n" <> cIndent 1 <> "void* pair = (x->"<> cVarPrefix <> morphismToCIdentifier sm (powerObjectLeg t)  <> "->pair_constructor)(x->"<> cVarPrefix <> morphismToCIdentifier sm (powerObjectLeg t) <> "->first_arg,x->"<> cVarPrefix <> morphismToCIdentifier sm (internalDomainLeg t) <>");\n" <> cIndent 1 <> "r = (x->"<> cVarPrefix <> morphismToCIdentifier sm (powerObjectLeg t) <> "->uncurried_function_pointer)(pair);\n" <> cIndent 1 <> "return r;\n}\n\n"
        (IdentityMorphism m) -> ";\n"
        (CompositeMorphism decomp) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x){\n"<> cIndent 1 <> "return " <> T.concat ((<> "(") <$> (morphismToCIdentifier sm) <$> decomp) <> "x" <> T.replicate (length decomp) ")" <> ";\n}\n\n"
        (BindingCone c) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x){\n"<> cIndent 1 <> objectToCIdentifier sm (target morph) <> "* y = malloc(sizeof("<> objectToCIdentifier sm (target morph) <> "));\n" <> (T.concat  $ setToList [ cIndent 1 <> "y->" <> cVarPrefix <> morphismToCIdentifier sm (legsCone c =>$ i) <> " = " <>  T.concat ((<> "(") <$> (morphismToCIdentifier sm) <$> decomp i) <> "x" <> T.replicate (length $ decomp i) ")" <> ";\n" | i <- topObjects]) <> cIndent 1 <> "return y;\n}\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
                decomp i = decompose (universeCategoryCone c) ((legsCone c) =>$ i @ morph)
        (BindingCocone cc) -> objectToCIdentifier sm (target morph) <> "* " <> morphismToCIdentifier sm morph <>"("<> objectToCIdentifier sm (source morph) <> "* x){\n"<> cIndent 1 <> "switch (x->current_type) {\n" <> (T.concat $ setToList [ cIndent 2 <> "case " <> (T.pack $ show $ fromJust $ elemIndex i $ Set.setToList $ ob $ indexingCategoryCocone cc) <> ":\n" <> cIndent 3 <> "return " <> T.concat ((<> "(") <$> (morphismToCIdentifier sm) <$> decomp i) <> "x->" <> cVarPrefix <> morphismToCIdentifier sm (legsCocone cc =>$ i) <> T.replicate (length $ decomp i) ")" <> ";\n" <> cIndent 3 <> "break;\n" | i <- bottomObjects]) <> cIndent 1 <> "}\n}\n\n"
            where
                fromJust (Just x) = x
                bottomObjects = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArToWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
                decomp i = decompose (universeCategoryCocone cc) (morph @ (legsCocone cc =>$ i))
        (BindingTripod t powLeg domLeg codomLeg) -> objectToCIdentifier sm (source powLeg) <> "* pair_constructor_" <> morphismToCIdentifier sm morph <> "(" <> objectToCIdentifier sm (target powLeg) <> "* x, " <> objectToCIdentifier sm (target domLeg) <> "* y){\n" <> cIndent 1 <> objectToCIdentifier sm (source powLeg) <> "* r = malloc(sizeof(" <> objectToCIdentifier sm (source powLeg) <> "));\n" <> cIndent 1 <> "r->" <> cVarPrefix <> morphismToCIdentifier sm powLeg <> " = x;\n" <> cIndent 1 <> "r->" <> cVarPrefix <> morphismToCIdentifier sm domLeg <> " = y;\n" <> cIndent 1 <> "return r;\n}\n" <> objectToCIdentifier sm (powerObject t) <> "* "<>  morphismToCIdentifier sm morph <> "(" <> objectToCIdentifier sm (internalDomain t) <> "* x){\n" <> cIndent 1 <> objectToCIdentifier sm (powerObject t) <> "* r = malloc(sizeof(" <> objectToCIdentifier sm (powerObject t) <> "));\n" <> cIndent 1 <> "r->uncurried_function_pointer = (" <> objectToCIdentifier sm (target codomLeg) <> "* (*) (void*)) " <> morphismToCIdentifier sm codomLeg <> ";\n" <> cIndent 1 <> "r->pair_constructor = (void* (*) (void*," <> objectToCIdentifier sm (target domLeg) <> "*)) pair_constructor_" <> morphismToCIdentifier sm morph <> ";\n" <> cIndent 1 <> "r->first_arg = (void*) x;\n" <> cIndent 1 <> "return r;\n}\n\n"   
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in transformMorphismToCCode"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph