{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Transcompiler from Poule to Haskell.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transcompiler from Poule to Haskell.

-}

module Language.Poule.Transcompilers.Haskell
(
    haskellFunctionPrefix,
    haskellTypePrefix,
    transformObjectToHaskellCode,
    transformMorphismToHaskellCode,
)

where
    import              Language.Poule.Parser  as P  hiding (Arrow)
    import              Language.Poule.SemanticAnalyser
    import              Language.Poule.Precompiler
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    import qualified    Data.Text               as T
    
    import              GHC.Generics
    
    haskellTypePrefix :: T.Text
    haskellTypePrefix = "Poule_"
    
    -- | Transform an object of the target of a sketch morphism into a Haskell identifier.
    objectToHaskellIdentifier :: SketchMorphism SketchObject SketchArrow -> SketchObject -> T.Text
    objectToHaskellIdentifier sm obj = case role of
        (ImageObject (Object x)) -> x
        (ImageObject (TerminalObject _)) -> "()"
        (ImageObject (InitialObject _)) -> "Void"
        (ImageObject x@(Product aliases _))
            | Set.null aliases -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            | otherwise -> anElement aliases
        (ImageObject x@(Coproduct aliases _))
            | Set.null aliases -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            | otherwise -> anElement aliases
        (ImageObject x@(Power aliases _ _))
            | Set.null aliases -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            | otherwise -> anElement aliases
        -- TODO : check previous error during semantic analysis phase
        (ApexObject x) -> haskellTypePrefix <> objectToIdentifier (apex x)
        (NadirObject x) -> haskellTypePrefix <> objectToIdentifier (nadir x)
        (PowerObject x) -> haskellTypePrefix <> objectToIdentifier (powerObject x)
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
    
    -- | Transforms an object of the target of a sketch morphism into Haskell code.
    transformObjectToHaskellCode :: SketchMorphism SketchObject SketchArrow -> SketchObject -> T.Text
    transformObjectToHaskellCode sm obj = case role of
        (ImageObject _) -> ";\n"
        (ApexObject c) -> newApex
            where
                b = baseCone c
                topObjects = [i | i <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) i), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
                newApex = "data " <> objectToHaskellIdentifier sm obj <> " = " <> objectToHaskellIdentifier sm obj<>" "<> (T.concat [(objectToHaskellIdentifier sm (b ->$ i))<>" " | i <- Set.setToList topObjects])<>";\n"
        (NadirObject cc) -> newNadir
            where
                b = baseCocone cc
                bottomObjects = [i | i <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArFromWithoutId (indexingCategoryCocone cc) i), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
                newNadir = if Set.null bottomObjects then "data "<>objectToHaskellIdentifier sm obj else "data "<>objectToHaskellIdentifier sm obj<>" = "<> (T.intercalate " | " $ [objectToHaskellIdentifier sm obj<>"_"<> (let (Object x) = i in x) <>" "<>(objectToHaskellIdentifier sm (b ->$ i)) | i <- Set.setToList bottomObjects])<>";\n"
        (PowerObject t) -> "type "<>objectToHaskellIdentifier sm obj<>" = "<>(objectToHaskellIdentifier sm (internalDomain t))<>" -> "<>(objectToHaskellIdentifier sm (internalCodomain t))<>";\n"
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
            
        
    haskellFunctionPrefix :: T.Text
    haskellFunctionPrefix = "poule_"
            
    -- | Transform a non identity generating morphism of the target of a sketch morphism into a Haskell identifier.
    morphismToHaskellIdentifier :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    morphismToHaskellIdentifier sm morph
        | isComposite (underlyingCategory $ target sm) morph = error $ "morphismToHaskellIdentifier called on a composite morphism : " ++ (show morph)
        | otherwise = case role of
            (ImageMorphism x) -> case unsafeGetLabel x of
                (ElementaryArrow y) -> y
                (Projection obj i) -> "projection_" <> objectToHaskellIdentifier sm obj <> "_" <> (T.pack $ show i)
                (Coprojection obj i) -> objectToHaskellIdentifier sm obj <> "_" <> (T.pack $ show i)
                (EvalMap obj) -> "eval_" <> objectToHaskellIdentifier sm obj
            (ConeLeg c i) -> haskellFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (CoconeLeg cc i) -> objectToHaskellIdentifier sm (nadir cc) <> "_" <> let (Object x) = i in x
            (EvaluationMap t) -> haskellFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (IdentityMorphism m) -> error $ "identity morphism given to morphismToHaskellIdentifier : "<>show morph
            (CompositeMorphism decomp) -> haskellFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingCone c) -> haskellFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingCocone cc) -> haskellFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingTripod t powLeg domLeg codomLeg) -> haskellFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in morphismToHaskellIdentifier"
            where
                UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
         
         
    -- | Transforms a generating morphism of the target of a sketch morphism into Haskell code.
    transformMorphismToHaskellCode :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    transformMorphismToHaskellCode sm morph = case role of
        (ImageMorphism _) -> ";\n"
        (ConeLeg c i) -> typeAnnotation <> morphismToHaskellIdentifier sm morph <>" ("<>(objectToHaskellIdentifier sm (apex c))<> T.concat (setToList[if i == j then " x" else " _" | j <- topObjects]) <>") = x;\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
        (CoconeLeg cc i) -> ";\n\n"
        (EvaluationMap t) -> typeAnnotation <> morphismToHaskellIdentifier sm morph <> " (" <> (objectToHaskellIdentifier sm (apex $ twoCone t)) <> " function x) = function x;\n\n"
        (IdentityMorphism m) -> ";\n"
        (CompositeMorphism decomp) -> typeAnnotation <> morphismToHaskellIdentifier sm morph <> " = " <> T.intercalate " . " ((morphismToHaskellIdentifier sm) <$> decomp) <> ";\n\n"
        (BindingCone c) -> typeAnnotation <> morphismToHaskellIdentifier sm morph <> " x = " <> objectToHaskellIdentifier sm (apex c) <> " " <> T.intercalate " " (Set.setToList ["(" <> (T.intercalate " . " $ morphismToHaskellIdentifier sm <$> (decompose (universeCategoryCone c) ((legsCone c) =>$ i @ morph))) <> " $ x)" | i <- topObjects]) <> ";\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
        (BindingCocone cc) -> typeAnnotation <> if Set.null bottomObjects then morphismToHaskellIdentifier sm morph <> " _ = undefined" else T.concat (setToList [morphismToHaskellIdentifier sm morph <> " (" <> morphismToHaskellIdentifier sm (legsCocone cc =>$ i) <> " x) = " <> T.intercalate " . " (morphismToHaskellIdentifier sm <$> (decompose (universeCategoryCocone cc) (morph @ (legsCocone cc =>$ i)))) <> " $ x;\n" | i <- bottomObjects]) <> "\n"
            where
                bottomObjects = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArToWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
        (BindingTripod t powLeg domLeg codomLeg) -> if target domLeg == target morph then typeAnnotation <> morphismToHaskellIdentifier sm morph <> " x y = " <> morphismToHaskellIdentifier sm codomLeg <> " (" <> objectToHaskellIdentifier sm (source codomLeg) <> " x y);\n\n" else typeAnnotation <> morphismToHaskellIdentifier sm morph <> " y x = " <> morphismToHaskellIdentifier sm codomLeg <> " (" <> objectToHaskellIdentifier sm (source codomLeg) <> " x y);\n\n"
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in transformMorphismToHaskellCode"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
            typeAnnotation = morphismToHaskellIdentifier sm morph <>" :: "<>(objectToHaskellIdentifier sm (source morph))<> " -> " <>  objectToHaskellIdentifier sm (target morph) <> ";\n"