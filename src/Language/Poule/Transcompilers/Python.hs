{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : Transcompiler from Poule to Python.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transcompiler from Poule to Python.

-}

module Language.Poule.Transcompilers.Python
(
    pythonIndent,
    pythonFunctionPrefix,
    transformObjectToPythonCode,
    transformMorphismToPythonCode,
)

where
    import              Language.Poule.Parser  as P  hiding (Arrow)
    import              Language.Poule.SemanticAnalyser
    import              Language.Poule.Precompiler
    
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.CartesianClosedCategory
    import              Math.Categories.FinGrph
    import              Math.Categories.CommaCategory
    import              Math.FiniteCategories.One
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Categories.FunctorCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.Categories.ConeCategory
    import              Math.Categories.FinSketch
    import              Math.IO.PrettyPrint

    import qualified    Data.Text               as  T
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    
    import              GHC.Generics
    
    pythonIndent :: Int -> T.Text
    pythonIndent 0 = ""
    pythonIndent x = "    "<>(pythonIndent $ x-1)

    
    -- | Transforms an object of the target of a sketch morphism into Python code.
    transformObjectToPythonCode :: SketchMorphism SketchObject SketchArrow -> SketchObject -> T.Text
    transformObjectToPythonCode sm obj = case role of
        (ImageObject _) -> "\n"
        (ApexObject c) -> "\n"
        (NadirObject cc) -> "\n"
        (PowerObject t) -> "\n"
        where
            UniquelyDeterminedObject role = getObjectDeterminacy sm obj
            
        
    pythonFunctionPrefix :: T.Text
    pythonFunctionPrefix = "poule_"
            
    -- | Transform a non identity generating morphism of the target of a sketch morphism into a Haskell identifier.
    morphismToPythonIdentifier :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    morphismToPythonIdentifier sm morph
        | isComposite (underlyingCategory $ target sm) morph = error $ "morphismToPythonIdentifier called on a composite morphism : " ++ (show morph)
        | otherwise = case role of
            (ImageMorphism x) -> case unsafeGetLabel x of
                (ElementaryArrow y) -> y
                (Projection (Object x) i) -> "projection_" <> x <> "_" <> (T.pack $ show i)
                (Coprojection (Object x) i) -> "coprojection_" <> x <> "_" <> (T.pack $ show i)
                (EvalMap (Object x)) -> "eval_" <> x
                (Projection x _) -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
                (Coprojection x _) -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
                (EvalMap x) -> error $ "Composite object in the source sketch should all be type aliased to specify their image in the model.\nThe object " ++ (show x) ++ " is not type aliased."
            (ConeLeg c i) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (CoconeLeg cc i) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (EvaluationMap t) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (IdentityMorphism m) -> error $ "identity morphism given to morphismToPythonIdentifier : "<>show morph
            (CompositeMorphism decomp) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingCone c) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingCocone cc) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (BindingTripod t powLeg domLeg codomLeg) -> pythonFunctionPrefix <> arrowToIdentifier (unsafeGetLabel morph)
            (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in morphismToPythonIdentifier"
            where
                UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph
            
    -- | Transforms a generating morphism of the target of a sketch morphism into Python code.
    transformMorphismToPythonCode :: SketchMorphism SketchObject SketchArrow -> CGMorphism SketchObject SketchArrow -> T.Text
    transformMorphismToPythonCode sm morph = case role of
        (ImageMorphism _) -> "\n"
        (ConeLeg c i) -> "def "<> morphismToPythonIdentifier sm morph <>"(x):\n" <> pythonIndent 1 <> "return x[\"" <> morphismToPythonIdentifier sm morph <> "\"]\n\n"
        (CoconeLeg cc i) -> "def "<> morphismToPythonIdentifier sm morph <>"(x):\n" <> pythonIndent 1 <> "return {\"" <> morphismToPythonIdentifier sm morph <> "\" : x}\n\n"
        (EvaluationMap t) -> "def "<> morphismToPythonIdentifier sm morph <> "(x):\n" <> pythonIndent 1 <> "return x[\"" <> morphismToPythonIdentifier sm (powerObjectLeg t) <> "\"](x[\""<> morphismToPythonIdentifier sm (internalDomainLeg t) <>"\"])\n\n"
        (IdentityMorphism m) -> "\n"
        (CompositeMorphism decomp) -> "def " <> morphismToPythonIdentifier sm morph <> "(x):\n" <> pythonIndent 1 <> "return " <> (T.concat ((<>"(") <$> (morphismToPythonIdentifier sm) <$> decomp)) <> "x" <> T.replicate (length decomp) ")" <> "\n\n"
        (BindingCone c) -> "def " <> morphismToPythonIdentifier sm morph <> "(x):\n" <> pythonIndent 1 <> "y = {}\n" <> pythonIndent 1 <> T.intercalate ("\n"<>pythonIndent 1) (Set.setToList ["y[\"" <> morphismToPythonIdentifier sm ((legsCone c) =>$ i) <> "\"] = " <> (T.concat ((<>"(") <$> (morphismToPythonIdentifier sm) <$> (decompose (tgt $ underlyingFunctor sm) (((legsCone c) =>$ i) @ morph)))) <> "x" <> T.replicate (length (decompose (tgt $ underlyingFunctor sm) (((legsCone c) =>$ i) @ morph))) ")" | i <- topObjects]) <> "\n" <> pythonIndent 1 <> "return y\n\n"
            where
                topObjects = [j | j <- ob $ indexingCategoryCone c, Set.null [f | f <- (genArToWithoutId (indexingCategoryCone c) j), Set.null (genArWithoutId (indexingCategoryCone c) (target f) (source f))]]
        (BindingCocone cc) -> if Set.null bottomObjects then "def " <> morphismToPythonIdentifier sm morph <> "(x):\n" <> pythonIndent 1 <> "raise Exception('Absurd function called.')" else "def " <> morphismToPythonIdentifier sm morph <> "(x):\n" <> T.concat (setToList [pythonIndent 1 <> "if \"" <> morphismToPythonIdentifier sm (legsCocone cc =>$ i) <> "\" in x:\n" <> pythonIndent 2 <> "return " <> (T.concat ((<>"(") <$> (morphismToPythonIdentifier sm) <$> (decompose (tgt $ underlyingFunctor sm) (morph @ (legsCocone cc =>$ i))))) <> "x[\"" <> morphismToPythonIdentifier sm (legsCocone cc =>$ i) <> "\"]" <> T.replicate (length (decompose (tgt $ underlyingFunctor sm) (morph @ (legsCocone cc =>$ i)))) ")" <> "\n"  | i <- bottomObjects]) <> "\n"
            where
                bottomObjects = [j | j <- ob $ indexingCategoryCocone cc, Set.null [f | f <- (genArToWithoutId (indexingCategoryCocone cc) j), Set.null (genArWithoutId (indexingCategoryCocone cc) (target f) (source f))]]
        BindingTripod t powLeg domLeg codomLeg -> "def "<> morphismToPythonIdentifier sm morph <> "(a):\n" <> pythonIndent 1 <> "return lambda b : " <> morphismToPythonIdentifier sm codomLeg <> "({\""<> morphismToPythonIdentifier sm powLeg <> "\" : a, \"" <> morphismToPythonIdentifier sm domLeg <> "\" : b})\n\n"
        (CompositionLawEntry rp1 rp2) -> error "CompositionLawEntry in transformMorphismToPythonCode"
        where
            UniquelyDeterminedMorphism role = getMorphismDeterminacy sm morph