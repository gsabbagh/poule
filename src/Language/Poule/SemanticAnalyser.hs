{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

{-| Module  : poule
Description : SemanticAnalyser for Poule.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Transforms an identifier table into sketches, sketch morphisms and models.

-}

module Language.Poule.SemanticAnalyser
(
    -- * Semantic data types
    SemanticError(..),
    SemanticToken(..),
    SemanticProgram(..),
    -- * Semantic analysis
    analyseSemantically,
    constructSemanticSketch,
    constructSemanticSketchMorphism,
)

where
    import              Math.Categories.FinGrph as G
    import              Math.Categories.FinSketch
    import              Math.Categories.ConeCategory
    import              Math.Categories.FunctorCategory
    import qualified    Math.CocompleteCategory as      CC
    import              Math.CartesianClosedCategory
    import              Math.FiniteCategories.CompositionGraph
    import              Math.FiniteCategories.DiscreteTwo
    import              Math.Category
    import              Math.FiniteCategory
    import              Math.Categories.FinCat
    import              Math.IO.PrettyPrint

    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as   Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as   Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust, catMaybes, listToMaybe)
    import qualified    Data.Text               as T
    import              Data.Text                   (pack)
    import              Data.List                   (sortBy)
    
    import              Language.Poule.Parser
    import              Language.Poule.Preprocesser
    
    import              GHC.Generics

    type FunctorSketch n e = FinFunctor (CategorySketch n e) (ArrowSketch n e) (ObjectSketch n e)
    
    -- | An error occurring during semantic analysis.
    data SemanticError = UnknownIdentifier FilePath Identifier
                       | RecursiveDefinition (FilePath,Identifier) [(FilePath,Identifier)]
                       | MismatchType String
                       
                       | SketchErrorTwoObjectsHaveSameAlias FilePath Identifier T.Text SketchObject SketchObject
                       | SketchErrorArrowNameConflict FilePath Identifier (Arrow SketchObject SketchArrow) (Arrow SketchObject SketchArrow)
                       | SketchErrorUnknownArrow FilePath Identifier SketchArrow
                       | SketchErrorUncompatibleComposition FilePath Identifier (Arrow SketchObject SketchArrow) (Arrow SketchObject SketchArrow)
                       | SketchErrorDifferentSourceInLawEntry FilePath Identifier [Arrow SketchObject SketchArrow] [Arrow SketchObject SketchArrow]
                       | SketchErrorDifferentTargetInLawEntry FilePath Identifier [Arrow SketchObject SketchArrow] [Arrow SketchObject SketchArrow]
                       | SketchErrorMultiEqualizerWrongSource FilePath Identifier SketchObject [SketchArrow]
                       | SketchErrorMultiEqualizerDifferentTarget FilePath Identifier SketchObject [SketchArrow] [SketchArrow]
                       | SketchErrorMultiCoequalizerWrongTarget FilePath Identifier SketchObject [SketchArrow]
                       | SketchErrorMultiCoequalizerDifferentSource FilePath Identifier SketchObject [SketchArrow] [SketchArrow]
                       
                       | SketchMorphismErrorMissingObjectImage FilePath Identifier SketchObject
                       | SketchMorphismErrorMissingArrowImage FilePath Identifier SketchArrow
                       | SketchMorphismDeclarationError FilePath Identifier (SketchMorphismError SketchObject SketchArrow)
                       | SketchMorphismInclusionError FilePath Identifier (SketchMorphismError SketchObject SketchArrow)
                       | SketchMorphismInclusionErrorObjectNotInImage FilePath Identifier SketchObject
                       | SketchMorphismInclusionErrorArrowNotInImage FilePath Identifier SketchArrow
                       
                       | ModelErrorInstancesOfDifferentLanguages FilePath Identifier (Identifier,SupportedTargetLanguage) (Identifier,SupportedTargetLanguage)
        deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)

    -- | A semantic token is either a sketch or a sketch morphism.
    data SemanticToken = SemanticSketch (Sketch SketchObject SketchArrow)
                       | SemanticSketchMorphism (SketchMorphism SketchObject SketchArrow)
                       | SemanticModel SupportedTargetLanguage T.Text
        deriving (Eq, Show, Generic, Simplifiable, PrettyPrint)

    -- | A semantic program is a mapping from identifier to 'SemanticToken's.
    type SemanticProgram = Map (FilePath, Identifier) SemanticToken

    -- | Take a list l and a function f and return every element x of l such that another element y of l has the same image as x by f.
    duplicates :: (Eq a, Eq b) => [a] -> (a -> b) -> [(a,a)]
    duplicates xs f = [(x1,x2) | x1 <- xs, x2 <- xs, x1 /= x2, f x1 == f x2]

    -- | Return all identifier a token depends on given an 'IdentifierTable' and a blacklist.
    dependencies :: IdentifierTable -> [(FilePath,Identifier)] -> (FilePath,Identifier) -> Either SemanticError [(FilePath,Identifier)]
    dependencies idTable blacklist (fp,i)
        | (fp,i) `elem` blacklist = Right []
        | not $ (fp,i) `isIn` (keys' idTable) = Left $ UnknownIdentifier fp i
        | otherwise = case idTable |!| (fp,i) of
            (ExternalRessource f j) -> dependencies idTable newBlackList (f,j)
            (InternalRessource (Sketch (SketchDeclaration _ st))) -> concat <$> (sequence $ sketchStatementHandling <$> st)
            (InternalRessource (SketchMorphism (SketchMorphismDeclaration _ sk1 sk2 _))) -> do
                sk1d <- dependencies idTable newBlackList (fp,sk1)
                sk2d <- dependencies idTable newBlackList (fp,sk2)
                return $ [(fp,sk1),(fp,sk2)] ++ sk1d ++ sk2d
            (InternalRessource (SketchMorphism (SketchMorphismInclusion _ sk1 sk2))) -> do
                sk1d <- dependencies idTable newBlackList (fp,sk1)
                sk2d <- dependencies idTable newBlackList (fp,sk2)
                return $ [(fp,sk1),(fp,sk2)] ++ sk1d ++ sk2d
            (InternalRessource (Model (ModelDeclaration _ sk _ insts _))) -> do
                skd <- dependencies idTable newBlackList (fp,sk)
                instsd <- sequence $ (\x -> dependencies idTable newBlackList (fp,x)) <$> insts
                return $ [(fp,sk)] ++ [(fp,inst) | inst <- insts] ++ skd ++ concat instsd
            (InternalRessource (Import _)) -> error "Import declaration is an internal ressource."
        where
            newBlackList = (fp,i) : blacklist
            sketchStatementHandling (Instance classSm _) =  ((fp,classSm):) <$> dependencies idTable newBlackList (fp,classSm)
            sketchStatementHandling _ = Right []
            
            
    -- | Checks if there is a circular definition in an 'IdentifierTable'.
    checkRecursiveDefinition :: IdentifierTable -> Either SemanticError ()
    checkRecursiveDefinition idTable
        | null dependencyOrErr = Left err
        | null recursiceDependency = Right ()
        | otherwise = Left $ uncurry RecursiveDefinition $ head recursiceDependency 
        where
            dependencyOrErr =  sequence $ (\x -> sequence (x,dependencies idTable [] x)) <$> Map.keys idTable
            Left err = dependencyOrErr
            Right dependency = dependencyOrErr
            recursiceDependency = filter (uncurry elem) dependency
        
    constructAnIdentifier :: IdentifierTable -> (FilePath,Identifier) -> Either SemanticError SemanticToken
    constructAnIdentifier idTable (fp,i)
        | not $ (fp,i) `isIn` (keys' idTable) = Left $ UnknownIdentifier fp i
        | otherwise = case token of
            (ExternalRessource f y) -> constructAnIdentifier idTable (f,y)
            (InternalRessource (Sketch _)) -> constructSemanticSketch idTable (fp,i)
            (InternalRessource (SketchMorphism _)) -> constructSemanticSketchMorphism idTable (fp,i)
            (InternalRessource (Model _)) -> constructSemanticModel idTable (fp,i)
        where
            token = idTable |!| (fp,i)
            
    -- | Takes a list of falcon tokens parsed to produce a semantically analysed program.
    analyseSemantically :: IdentifierTable -> Either SemanticError SemanticProgram
    analyseSemantically idTable = do
        recursiveDefinition <- checkRecursiveDefinition idTable
        result <- sequence $ (\x -> sequence (x,constructAnIdentifier idTable x)) <$> (Map.keys idTable)
        return $ weakMap result
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
    
    -- * SEMANTIC SKETCH
    
    type PreAliasTable = [(T.Text,SketchObject)]
    
    type AliasTable = Map T.Text SketchObject
    
    -- ** Construct sketch objects
    
    constructSketchObjects :: (FilePath, Identifier) -> [SketchStatement] -> Either SemanticError (Set SketchObject)
    constructSketchObjects (fp,i) stmts
        | not $ null maybeErrAliasTable = Left $ fromJust maybeErrAliasTable
        | otherwise = Right $ objectsWithoutAliases
        where
            preAliasTable = foldr constructPreAliasTable [] stmts
            maybeErrAliasTable = checkPreAliasTable fp i preAliasTable
            aliasTable = weakMap preAliasTable
            extractedObjects = Set.concat2 $ extractInnerObjectsFromObjects <$> (Set.unions $ extractObjectsFromSketchStatement <$> stmts)
            extractedObjectsWithAliases = simplify $ enrichObjectWithAliases aliasTable <$> extractedObjects
            objectsWithoutAliases = Set.filter (\x -> not $ x `isIn` [Object k | k <- keys' aliasTable]) extractedObjectsWithAliases
    
    constructPreAliasTable :: SketchStatement -> PreAliasTable -> PreAliasTable
    constructPreAliasTable (TypeAlias t o) pat =  (t,o):pat
    constructPreAliasTable _ pat = pat
    
    checkPreAliasTable :: FilePath -> Identifier -> PreAliasTable -> Maybe SemanticError
    checkPreAliasTable fp i pat
        | not $ null $ dupAlias = Just $ SketchErrorTwoObjectsHaveSameAlias fp i (fst $ fst $ head dupAlias) (snd $ fst $ head dupAlias) (snd $ snd $ head dupAlias)
        | otherwise = Nothing
        where
            dupAlias = filter (uncurry (/=)) $ duplicates pat fst
            
    extractObjectsFromSketchStatement :: SketchStatement -> Set SketchObject
    extractObjectsFromSketchStatement (NewObject x) = set [x]
    extractObjectsFromSketchStatement (NewArrow _ x y) = set [x,y]
    extractObjectsFromSketchStatement (MultiEqualizer eq prev _) = set [eq, prev]
    extractObjectsFromSketchStatement (MultiCoequalizer eq prev _) = set [eq, prev]
    extractObjectsFromSketchStatement (TypeAlias alias obj) = set [obj]
    extractObjectsFromSketchStatement _ = set []
    
    extractInnerObjectsFromObjects :: SketchObject -> Set SketchObject
    extractInnerObjectsFromObjects x@(Object _) = set [x]
    extractInnerObjectsFromObjects x@(TerminalObject _) = set [x]
    extractInnerObjectsFromObjects x@(InitialObject _) = set [x]
    extractInnerObjectsFromObjects x@(Product _ objs) = set [x] ||| set objs ||| (Set.unions $ extractInnerObjectsFromObjects <$> objs)
    extractInnerObjectsFromObjects x@(Coproduct _ objs) = set [x] ||| set objs ||| (Set.unions $ extractInnerObjectsFromObjects <$> objs)
    extractInnerObjectsFromObjects x@(Power _ dom codom) = set [x, dom, codom, Product (set []) [x,dom]] ||| extractInnerObjectsFromObjects dom ||| extractInnerObjectsFromObjects codom 
   
    enrichObjectWithAliases :: AliasTable -> SketchObject -> SketchObject
    enrichObjectWithAliases at y@(Object t) = Object t
    enrichObjectWithAliases at y@(TerminalObject _) = TerminalObject [x | x <- keys' at, at |!| x == y]
    enrichObjectWithAliases at y@(InitialObject _) = InitialObject [x | x <- keys' at, at |!| x == y]
    enrichObjectWithAliases at y@(Product _ objs) = Product [x | x <- keys' at, at |!| x == y] (enrichObjectWithAliases at <$> objs)
    enrichObjectWithAliases at y@(Coproduct _ objs) = Coproduct [x | x <- keys' at, at |!| x == y] (enrichObjectWithAliases at <$> objs)
    enrichObjectWithAliases at y@(Power _ dom codom) = Power [x | x <- keys' at, at |!| x == y] (enrichObjectWithAliases at dom) (enrichObjectWithAliases at codom)
    
    getAliases :: SketchObject -> Set T.Text
    getAliases (Object _) = set []
    getAliases (TerminalObject xs) = xs
    getAliases (InitialObject xs) = xs
    getAliases (Product xs _) = xs
    getAliases (Coproduct xs _) = xs
    getAliases (Power xs _ _) = xs
    
    isParsedObjectAlias :: Set SketchObject -> SketchObject -> Bool
    isParsedObjectAlias sketchObjects (Object x) = not $ Set.null res
        where
            res = [o | o <- sketchObjects, x `isIn` getAliases o]
    
    parsedObjectToSketchObject :: Set SketchObject -> SketchObject -> SketchObject
    parsedObjectToSketchObject sketchObjects (Object x)
        | isParsedObjectAlias sketchObjects (Object x) = anElement [o | o <- sketchObjects, x `isIn` getAliases o]
        | otherwise = (Object x)
    parsedObjectToSketchObject sketchObjects x@(Product _ xs)
        | Set.null productWithSameBody = x
        | otherwise = anElement productWithSameBody
        where
            extractFromProduct (Product _ ys) = ys
            extractFromProduct _ = []
            productWithSameBody = [o | o <- sketchObjects, xs == extractFromProduct o]
    parsedObjectToSketchObject sketchObjects x@(TerminalObject _)
        | Set.null terminalInsketchObjects = x
        | otherwise = anElement terminalInsketchObjects
        where
            isTerminal (TerminalObject _) = True
            isTerminal _ = False
            terminalInsketchObjects = [o | o <- sketchObjects, isTerminal o]
    parsedObjectToSketchObject sketchObjects x@(InitialObject _)
        | Set.null initialInsketchObjects = x
        | otherwise = anElement initialInsketchObjects
        where
            isInitial (InitialObject _) = True
            isInitial _ = False
            initialInsketchObjects = [o | o <- sketchObjects, isInitial o]
    parsedObjectToSketchObject sketchObjects x@(Coproduct _ xs)
        | Set.null coproductWithSameBody = x
        | otherwise = anElement coproductWithSameBody
        where
            extractFromCoproduct (Coproduct _ ys) = ys
            extractFromCoproduct _ = []
            coproductWithSameBody = [o | o <- sketchObjects, xs == extractFromCoproduct o]
    parsedObjectToSketchObject sketchObjects x@(Power _ dom codom)
        | Set.null powerWithSameBody = x
        | otherwise = anElement powerWithSameBody
        where
            extractFromPower (Power _ dom_ codom_) = [dom_,codom_]
            extractFromPower _ = []
            powerWithSameBody = [o | o <- sketchObjects, [dom,codom] == extractFromPower o]
    
    -- ** Construct sketch arrows
    
    constructSketchArrows :: (FilePath, Identifier) -> Set SketchObject -> [SketchStatement] -> Either SemanticError (Set (Arrow SketchObject SketchArrow))
    constructSketchArrows (fp,i) sketchObjects stmts
        | not $ null dups = Left $ (uncurry $ SketchErrorArrowNameConflict fp i) (head dups)
        | otherwise = Right $ arrowsToCreate
        where
            newArrows = Set.unions $ extractNewArrows sketchObjects <$> stmts
            inducedArrows = simplify $ (Set.concat2 $ extractInducedArrows sketchObjects <$> sketchObjects)
            arrowsToCreate = newArrows `Set.union` inducedArrows
            dups = duplicates (setToList arrowsToCreate) labelArrow
    
    extractNewArrows :: Set SketchObject -> SketchStatement -> Set (Arrow SketchObject SketchArrow)
    extractNewArrows sketchObjects (NewArrow n s t) = set [Arrow{labelArrow = n, sourceArrow = parsedObjectToSketchObject sketchObjects s, targetArrow = parsedObjectToSketchObject sketchObjects t}]
    extractNewArrows sketchObjects (MultiEqualizer a prev _) = set [Arrow{labelArrow = Projection (parsedObjectToSketchObject sketchObjects a) 0, sourceArrow = parsedObjectToSketchObject sketchObjects a, targetArrow = parsedObjectToSketchObject sketchObjects prev}]
    extractNewArrows sketchObjects (MultiCoequalizer n prev _) = set [Arrow{labelArrow = Coprojection (parsedObjectToSketchObject sketchObjects n) 0, sourceArrow = parsedObjectToSketchObject sketchObjects prev, targetArrow = parsedObjectToSketchObject sketchObjects n}]
    extractNewArrows _ _ = set []
    
    extractInducedArrows :: Set SketchObject -> SketchObject -> Set (Arrow SketchObject SketchArrow)
    extractInducedArrows sketchObjects x@(Object _) = set []
    extractInducedArrows sketchObjects (InitialObject _) = set []
    extractInducedArrows sketchObjects (TerminalObject _) = set []
    extractInducedArrows sketchObjects x@(Product _ objs) = set [Arrow{labelArrow = Projection x i, sourceArrow = x, targetArrow = parsedObjectToSketchObject sketchObjects (objs !! i)} | i <- [0..(length objs - 1)]]
    extractInducedArrows sketchObjects x@(Coproduct _ objs) = set [Arrow{labelArrow = Coprojection x i, sourceArrow = parsedObjectToSketchObject sketchObjects (objs !! i), targetArrow = x} | i <- [0..(length objs - 1)]]
    extractInducedArrows sketchObjects x@(Power _ d c) = set [Arrow{labelArrow = EvalMap (parsedObjectToSketchObject sketchObjects $ Product (set []) [x, d]), sourceArrow = parsedObjectToSketchObject sketchObjects $ Product (set []) [x, d], targetArrow = parsedObjectToSketchObject sketchObjects c}]
    
    parsedLabelArrowToSketchArrow :: (FilePath, Identifier) -> Set SketchObject -> Set (Arrow SketchObject SketchArrow) -> SketchArrow -> Either SemanticError (Arrow SketchObject SketchArrow)
    parsedLabelArrowToSketchArrow (fp,identif) parsedObjects parsedArrows x@(ElementaryArrow _)
        | Set.null result = Left $ SketchErrorUnknownArrow fp identif x
        | otherwise = Right $ anElement result
        where
            result = [a | a <- parsedArrows, labelArrow a == x]
    parsedLabelArrowToSketchArrow (fp,identif) parsedObjects parsedArrows x@(Projection obj i)
        | skObj `isIn` parsedObjects && not (Set.null result) = Right $ anElement result
        | otherwise = Left $ SketchErrorUnknownArrow fp identif x
        where
            skObj = parsedObjectToSketchObject parsedObjects obj
            result = [a | a <- parsedArrows, labelArrow a == (Projection skObj i)]
    parsedLabelArrowToSketchArrow (fp,identif) parsedObjects parsedArrows x@(Coprojection obj i)
        | skObj `isIn` parsedObjects && not (Set.null result) = Right $ anElement result 
        | otherwise = Left $ SketchErrorUnknownArrow fp identif x
        where
            skObj = parsedObjectToSketchObject parsedObjects obj
            result = [a | a <- parsedArrows, labelArrow a == (Coprojection skObj i)]
    parsedLabelArrowToSketchArrow (fp,identif) parsedObjects parsedArrows x@(EvalMap obj)
        | skObj `isIn` parsedObjects && not (Set.null result) = Right $ anElement result
        | otherwise = Left $ SketchErrorUnknownArrow fp identif x
        where
            skObj = parsedObjectToSketchObject parsedObjects obj
            result = [a | a <- parsedArrows, labelArrow a == (EvalMap skObj)]
            
    parsedLabelArrowToCGMorphism :: (FilePath, Identifier) -> CompositionGraph SketchObject SketchArrow -> SketchArrow -> Either SemanticError (CGMorphism SketchObject SketchArrow)
    parsedLabelArrowToCGMorphism (fp,identif) cg arr = do
        arrow <- parsedLabelArrowToSketchArrow (fp,identif) (ob cg) (unsafeCGMorphismToArrow <$> genArrowsWithoutId cg) arr
        return $ unsafeArrowToCGMorphism cg arrow
    
    -- ** Construct composition law
    
    constructCompositionLaw :: (FilePath, Identifier) -> Set SketchObject -> Set (Arrow SketchObject SketchArrow) -> [SketchStatement] -> Either SemanticError (CompositionLaw SketchObject SketchArrow)
    constructCompositionLaw (fp,i) sketchObjects sketchArrows stmts = do
        lawEntries <- sequence $ extractLawEntries (fp,i) sketchObjects sketchArrows <$> stmts
        return $ weakMap $ catMaybes lawEntries
    
    
    extractLawEntries :: (FilePath, Identifier) -> Set SketchObject -> Set (Arrow SketchObject SketchArrow) -> SketchStatement -> Either SemanticError (Maybe ([Arrow SketchObject SketchArrow],[Arrow SketchObject SketchArrow]))
    extractLawEntries (fp,i) sketchObjects sketchArrows (LawEntry xs1 xs2) = do
        l <- sequence $ parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows <$> xs1
        r <- sequence $ parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows <$> xs2
        check1 <- sequence $ uncurry checkCompatibility <$> zip (init l) (tail l)
        check2 <- sequence $ uncurry checkCompatibility <$> zip (init r) (tail r)
        check3 <- checkSourceAndTarget l r
        return $ Just $ (reverse l, reverse r)
        where
            checkCompatibility a b 
                | targetArrow a /= sourceArrow b = Left $ SketchErrorUncompatibleComposition fp i a b
                | otherwise = Right ()
            checkSourceAndTarget a b
                | sourceArrow (head a) /= sourceArrow (head b) = Left $ SketchErrorDifferentSourceInLawEntry fp i a b
                | targetArrow (last a) /= targetArrow (last b) = Left $ SketchErrorDifferentTargetInLawEntry fp i a b
                | otherwise = Right ()
    extractLawEntries _ _ _ _ = Right $ Nothing
    
    -- ** Checks for (co)equalizers
            
    checkEqualizers :: (FilePath, Identifier) -> Set SketchObject -> Set (Arrow SketchObject SketchArrow) -> SketchStatement -> Either SemanticError [()]
    checkEqualizers (fp,i) sketchObjects sketchArrows (MultiEqualizer a prev pairs) = sequence $ checkPair <$> pairs
        where
            checkPair (x,y) = do
                xFirstArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (head x)
                yFirstArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (head y)
                xLastArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (last x)
                yLastArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (last y)
                if sourceArrow xFirstArr /= prev then Left $ SketchErrorMultiEqualizerWrongSource fp i a x else do
                    if sourceArrow yFirstArr /= prev then Left $ SketchErrorMultiEqualizerWrongSource fp i a y else do
                        if targetArrow xLastArr /= targetArrow yLastArr then Left $ SketchErrorMultiEqualizerDifferentTarget fp i a x y else do
                            return $ ()
    checkEqualizers (fp,i) sketchObjects sketchArrows (MultiCoequalizer n prev pairs) = sequence $ checkPair <$> pairs
        where
            checkPair (x,y) = do
                xFirstArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (head x)
                yFirstArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (head y)
                xLastArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (last x)
                yLastArr <- parsedLabelArrowToSketchArrow (fp,i) sketchObjects sketchArrows (last y)
                if targetArrow xLastArr /= prev then Left $ SketchErrorMultiCoequalizerWrongTarget fp i n x else do
                    if targetArrow yLastArr /= prev then Left $ SketchErrorMultiCoequalizerWrongTarget fp i n y else do
                        if sourceArrow xFirstArr /= sourceArrow yFirstArr then Left $ SketchErrorMultiCoequalizerDifferentSource fp i n x y else do
                            return ()
    checkEqualizers _ _ _ _ = Right [()]
    
    
    -- ** Construct cones
    
    constructCones :: (FilePath,Identifier) -> CompositionGraph SketchObject SketchArrow -> [SketchStatement] -> Either SemanticError (Set (ConeSketch SketchObject SketchArrow))
    constructCones (fp,i) cg stmts = do
        conesFromEqualizers <- set <$> catMaybes <$> (sequence $ extractConesFromEqualizers (fp,i) cg <$> stmts)
        let conesFromProducts = Set.catMaybes $ extractConesFromProducts cg <$> ob cg
        return $ conesFromEqualizers ||| conesFromProducts
    
    extractConesFromEqualizers :: (FilePath, Identifier) -> CompositionGraph SketchObject SketchArrow -> SketchStatement -> Either SemanticError (Maybe (ConeSketch SketchObject SketchArrow))
    extractConesFromEqualizers (fp,identif) cg (MultiEqualizer a prev pairs) = do
            let indexingObjects = Set.insert (Object "Root") ((Object .pack.show) <$> set [0..(length pairs - 1)])
            let indexingArrows = Set.unions [set [Arrow{sourceArrow = Object "Root", targetArrow = Object $ pack $ show i, labelArrow = ElementaryArrow $ pack $ (show i ++ "_A")}, Arrow{sourceArrow = Object "Root", targetArrow = Object $ pack $ show i, labelArrow = ElementaryArrow $ pack $ (show i ++ "_B")}] | i <- [0..(length pairs - 1)]]
            let indexingCat = unsafeCompositionGraph (unsafeGraph indexingObjects indexingArrows) (weakMap [])
            omapWithoutRoot <- sequence [sequence (Object $ pack $ show $ i, target <$> (parsedLabelArrowToCGMorphism (fp,identif) cg (last $ fst $ pairs !! i))) | i <- [0..(length pairs - 1)]]
            mmap_A <- sequence [sequence (unsafeGetMorphismFromLabel indexingCat (ElementaryArrow $ pack $ (show i ++ "_A")), compose <$> reverse  <$> (sequence $ (parsedLabelArrowToCGMorphism (fp,identif) cg) <$> (fst $ pairs !! i))) | i <- [0..(length pairs - 1)]]
            mmap_B <- sequence [sequence (unsafeGetMorphismFromLabel indexingCat (ElementaryArrow $ pack $ (show i ++ "_B")), compose <$> reverse  <$> (sequence $ (parsedLabelArrowToCGMorphism (fp,identif) cg) <$> (snd $ pairs !! i))) | i <- [0..(length pairs - 1)]]
            let base = completeDiagram Diagram{
                src = indexingCat,
                tgt = cg,
                omap = weakMap $ (Object "Root", parsedObjectToSketchObject (ob cg) prev):omapWithoutRoot,
                mmap = weakMap $ mmap_A ++ mmap_B
                }
            let apexEq = parsedObjectToSketchObject (ob cg) a
            let nat = unsafeNaturalTransformation (constantDiagram indexingCat cg apexEq) base (weakMap [(Object $ pack $ "Root", unsafeGetMorphismFromLabel cg (Projection apexEq 0))])
            return $ Just $ completeCone $ unsafeCone apexEq nat
    extractConesFromEqualizers _ _ _ = Right $ Nothing
    
    extractConesFromProducts :: CompositionGraph SketchObject SketchArrow -> SketchObject -> Maybe (ConeSketch SketchObject SketchArrow)
    extractConesFromProducts cg a@(Product _ objs) = do
        let indexingCat = unsafeCompositionGraph (unsafeGraph ((Object .pack.show) <$> set [0..(length objs - 1)]) (set [])) (weakMap [])
        let base = completeDiagram Diagram{src = indexingCat, tgt = cg, omap = weakMap [(Object $ pack $ show $ i, parsedObjectToSketchObject (ob cg) (objs !! i)) | i <- [0..(length objs - 1)]], mmap = weakMap []}
        let nat = unsafeNaturalTransformation (constantDiagram indexingCat cg a) base (weakMap [(Object $ pack $ show $ i, unsafeGetMorphismFromLabel cg (Projection a i)) | i <- [0..(length objs - 1)]])
        return $ unsafeCone a nat
    extractConesFromProducts cg a@(TerminalObject _) = do
        let indexingCat = unsafeCompositionGraph (unsafeGraph (set []) (set [])) (weakMap [])
        let base = Diagram{src = indexingCat, tgt = cg, omap = weakMap [], mmap = weakMap []}
        let nat = unsafeNaturalTransformation (constantDiagram indexingCat cg a) base (weakMap [])
        return $ unsafeCone a nat
    extractConesFromProducts _ _ = Nothing
    
            
    -- ** Construct cocones
            
    constructCocones :: (FilePath,Identifier) -> CompositionGraph SketchObject SketchArrow -> [SketchStatement] -> Either SemanticError (Set (CoconeSketch SketchObject SketchArrow))
    constructCocones (fp,i) cg stmts = do
        coconesFromCoequalizers <- set <$> catMaybes <$> (sequence $ extractCoconesFromCoequalizers (fp,i) cg <$> stmts)
        let coconesFromCoproducts = Set.catMaybes $ extractCoconesFromCoproducts cg <$> ob cg
        return $ coconesFromCoequalizers ||| coconesFromCoproducts
    
    extractCoconesFromCoequalizers :: (FilePath, Identifier) -> CompositionGraph SketchObject SketchArrow -> SketchStatement -> Either SemanticError (Maybe (CoconeSketch SketchObject SketchArrow))
    extractCoconesFromCoequalizers (fp,identif) cg (MultiCoequalizer a prev pairs) = do
            let indexingObjects = Set.insert (Object "Root") ((Object .pack.show) <$> set [0..(length pairs - 1)])
            let indexingArrows = Set.unions [set [Arrow{targetArrow = Object "Root", sourceArrow = Object $ pack $ show i, labelArrow = ElementaryArrow $ pack $ (show i ++ "_A")}, Arrow{sourceArrow = Object "Root", targetArrow = Object $ pack $ show i, labelArrow = ElementaryArrow $ pack $ (show i ++ "_B")}] | i <- [0..(length pairs - 1)]]
            let indexingCat = unsafeCompositionGraph (unsafeGraph indexingObjects indexingArrows) (weakMap [])
            omapWithoutRoot <- sequence [sequence (Object $ pack $ show $ i, source <$> (parsedLabelArrowToCGMorphism (fp,identif) cg (last $ fst $ pairs !! i))) | i <- [0..(length pairs - 1)]]
            mmap_A <- sequence [sequence (unsafeGetMorphismFromLabel indexingCat (ElementaryArrow $ pack $ (show i ++ "_A")), compose <$> reverse  <$> (sequence $ (parsedLabelArrowToCGMorphism (fp,identif) cg) <$> (fst $ pairs !! i))) | i <- [0..(length pairs - 1)]]
            mmap_B <- sequence [sequence (unsafeGetMorphismFromLabel indexingCat (ElementaryArrow $ pack $ (show i ++ "_B")), compose <$> reverse  <$> (sequence $ (parsedLabelArrowToCGMorphism (fp,identif) cg) <$> (snd $ pairs !! i))) | i <- [0..(length pairs - 1)]]
            let base = completeDiagram Diagram{
                src = indexingCat,
                tgt = cg,
                omap = weakMap $ (Object "Root", parsedObjectToSketchObject (ob cg) prev):omapWithoutRoot,
                mmap = weakMap $ mmap_A ++ mmap_B
                }
            let nadirCoeq = parsedObjectToSketchObject (ob cg) a
            let nat = unsafeNaturalTransformation base (constantDiagram indexingCat cg nadirCoeq) (weakMap [(Object $ pack $ "Root", unsafeGetMorphismFromLabel cg (Coprojection nadirCoeq 0))])
            return $ Just $ completeCocone $ unsafeCocone nadirCoeq nat
    extractCoconesFromCoequalizers _ _ _ = Right $ Nothing
    
    extractCoconesFromCoproducts :: CompositionGraph SketchObject SketchArrow -> SketchObject -> Maybe (CoconeSketch SketchObject SketchArrow)
    extractCoconesFromCoproducts cg a@(Coproduct _ objs) = do
        let indexingCat = unsafeCompositionGraph (unsafeGraph ((Object .pack.show) <$> set [0..(length objs - 1)]) (set [])) (weakMap [])
        let base = completeDiagram Diagram{src = indexingCat, tgt = cg, omap = weakMap [(Object $ pack $ show $ i, parsedObjectToSketchObject (ob cg) (objs !! i)) | i <- [0..(length objs - 1)]], mmap = weakMap []}
        let nat = unsafeNaturalTransformation base (constantDiagram indexingCat cg a) (weakMap [(Object $ pack $ show $ i, unsafeGetMorphismFromLabel cg (Coprojection a i)) | i <- [0..(length objs - 1)]])
        return $ unsafeCocone a nat
    extractCoconesFromCoproducts cg a@(InitialObject _) = do
        let indexingCat = unsafeCompositionGraph (unsafeGraph (set []) (set [])) (weakMap [])
        let base = Diagram{src = indexingCat, tgt = cg, omap = weakMap [], mmap = weakMap []}
        let nat = unsafeNaturalTransformation base (constantDiagram indexingCat cg a) (weakMap [])
        return $ unsafeCocone a nat
    extractCoconesFromCoproducts _ _ = Nothing
      

    -- ** Construct tripods

    constructTripods :: (FilePath, Identifier) -> CompositionGraph SketchObject SketchArrow -> Either SemanticError (Set (TripodSketch SketchObject SketchArrow))
    constructTripods (fp,identif) cg = Right $ Set.catMaybes $ helper <$> ob cg
        where
            helper a@(Power _ d c) = Just $ unsafeTripod cone_ (unsafeGetMorphismFromLabel cg $ EvalMap prodObj)
                where
                    base = twoDiagram cg a (parsedObjectToSketchObject (ob cg) d)
                    prodObj = parsedObjectToSketchObject (ob cg) $ Product (set []) [a,parsedObjectToSketchObject (ob cg) d]
                    srcDiag = constantDiagram DiscreteTwo cg prodObj
                    nat = unsafeNaturalTransformation srcDiag base (weakMap [(A, unsafeGetMorphismFromLabel cg (Projection prodObj 0)),(B, unsafeGetMorphismFromLabel cg (Projection prodObj 1))])
                    cone_ = unsafeCone prodObj nat
            helper _ = Nothing
      
    -- ** Instances
     
    transformColimitObject :: Cocone (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch (CC.Colimit Int SketchObject) (CC.Colimit Int SketchArrow)) (SketchMorphism (CC.Colimit Int SketchObject) (CC.Colimit Int SketchArrow)) (Sketch (CC.Colimit Int SketchObject) (CC.Colimit Int SketchArrow)) -> CC.Colimit Int SketchObject -> SketchObject
    transformColimitObject colimCocone (CC.CoproductElement j obj)
        | j == 0 = obj
        | otherwise = case obj of
            (Object x)
                | (not $ (Object x) `isIn` unprojectedObjectsOfOriginalCategory) && functorFromOriginalCategory ->$ (CC.Coprojection $ parsedObjectToSketchObject unprojectedObjectsOfOriginalCategory (Object x)) == (CC.CoproductElement j obj)  -> (Object $ prependClass x) -- An alias mapped to itself by the cocone
                | not $ (Object x) `isIn` unprojectedObjectsOfOriginalCategory -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection $ parsedObjectToSketchObject unprojectedObjectsOfOriginalCategory (Object x))) -- An alias not mapped to itself by the cocone
                | functorFromOriginalCategory ->$ (CC.Coprojection (Object x)) == (CC.CoproductElement j obj) -> (Object $ prependClass x) -- not an alias mapped to itself
                | otherwise -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection (Object x))) -- not an alias not mapped to itself
            x@(TerminalObject aliases)
                | functorFromOriginalCategory ->$ (CC.Coprojection x) == (CC.CoproductElement j obj) -> (TerminalObject $ prependClass <$> aliases) -- mapped to itself
                | otherwise -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection x)) -- not mapped to itself
            x@(InitialObject aliases)
                | functorFromOriginalCategory ->$ (CC.Coprojection x) == (CC.CoproductElement j obj) -> (InitialObject $ prependClass <$> aliases) -- mapped to itself
                | otherwise -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection x)) -- not mapped to itself
            x@(Product aliases xs)
                | functorFromOriginalCategory ->$ (CC.Coprojection x) == (CC.CoproductElement j obj) -> (Product (prependClass <$> aliases) $ transformColimitObject colimCocone <$> CC.CoproductElement j <$> xs) -- mapped to itself
                | otherwise -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection x)) -- not mapped to itself
            x@(Coproduct aliases xs)     
                | functorFromOriginalCategory ->$ (CC.Coprojection x) == (CC.CoproductElement j obj) -> (Coproduct (prependClass <$> aliases) $ transformColimitObject colimCocone <$> CC.CoproductElement j <$> xs) -- mapped to itself
                | otherwise -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection x)) -- not mapped to itself
            x@(Power aliases dom codom)
                | functorFromOriginalCategory ->$ (CC.Coprojection x) == (CC.CoproductElement j obj) -> (Power (prependClass <$> aliases) (transformColimitObject colimCocone $ CC.CoproductElement j dom) (transformColimitObject colimCocone $ CC.CoproductElement j codom)) -- mapped to itself
                | otherwise -> transformColimitObject colimCocone (functorFromOriginalCategory ->$ (CC.Coprojection x)) -- not mapped to itself
        where
            prependClass x = "class_" <> (pack $ show $ j) <> "_" <> x
            functorFromOriginalCategory = (underlyingFunctor $ (legsCocone colimCocone) =>$ j)
            originalCategory = src functorFromOriginalCategory
            unprojectedObjectsOfOriginalCategory = CC.uncoproject <$> ob originalCategory
            
    transformColimitArrow :: Cocone (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch (CC.Colimit Int SketchObject) (CC.Colimit Int SketchArrow)) (SketchMorphism (CC.Colimit Int SketchObject) (CC.Colimit Int SketchArrow)) (Sketch (CC.Colimit Int SketchObject) (CC.Colimit Int SketchArrow)) -> CC.Colimit Int SketchArrow -> SketchArrow
    transformColimitArrow colimCocone (CC.CoproductElement j arr)
        | j == 0 = arr
        | otherwise = case arr of
            (ElementaryArrow x)
                | g (ElementaryArrow x) == (CC.CoproductElement j arr) -> (ElementaryArrow $ "class_" <> (pack $ show $ j) <> "_" <> x)
                | otherwise -> transformColimitArrow colimCocone $ g (ElementaryArrow x)
            (Projection obj k)
                | g (Projection obj k) == (CC.CoproductElement j arr) -> Projection (transformColimitObject colimCocone $ CC.CoproductElement j obj) k
                | otherwise -> transformColimitArrow colimCocone $ g (Projection obj k)
            (Coprojection obj k)
                | g (Coprojection obj k) == (CC.CoproductElement j arr) -> Coprojection (transformColimitObject colimCocone $ CC.CoproductElement j obj) k
                | otherwise -> transformColimitArrow colimCocone $ g (Coprojection obj k)
            (EvalMap obj)
                | g (EvalMap obj) == (CC.CoproductElement j arr) -> EvalMap (transformColimitObject colimCocone $ CC.CoproductElement j obj)
                | otherwise -> transformColimitArrow colimCocone $ g (EvalMap obj)
        where
            funct = (underlyingFunctor $ (legsCocone colimCocone) =>$ j)
            g x = if null $ mmap funct |?| (unsafeGetMorphismFromLabel (underlyingCategory $ (baseCocone colimCocone) ->$ j) (CC.Coprojection x)) then error "No image for coprojection morphism" else unsafeGetLabel $ funct ->£  (unsafeGetMorphismFromLabel (underlyingCategory $ (baseCocone colimCocone) ->$ j) (CC.Coprojection x))
      
    instantiate :: (FilePath,Identifier) -> IdentifierTable -> Sketch SketchObject SketchArrow -> [SketchStatement] -> Either SemanticError (Sketch SketchObject SketchArrow)
    instantiate (fp,identif) idTable originalSketch stmts = do
        diag <- constructInstantiationDiagram (fp,identif) idTable originalSketch stmts
        let colimCocone = CC.colimit diag
        let colim = nadir colimCocone
        let cgColim = underlyingCategory colim
        let diag1 = mapOnObjects2 (transformColimitObject colimCocone) cgColim
        let diag2 = mapOnArrows2 (transformColimitArrow colimCocone) (tgt diag1)
        let renamingDiag = diag2 <-@<- diag1

        let uncoprojectCone c = (mapOnArrows2 CC.uncoproject (tgt (mapOnObjects2 CC.uncoproject (src (baseCone c))))) <-@<- (mapOnObjects2 CC.uncoproject (src (baseCone c))) 
        let renameCone c = unsafeCone (renamingDiag ->$ (apex c)) (renamingDiag <-@<= (legsCone c) <=@<- (unsafeInverseDiagram $ uncoprojectCone c))
        let uncoprojectCocone cc = (mapOnArrows2 CC.uncoproject (tgt (mapOnObjects2 CC.uncoproject (src (baseCocone cc))))) <-@<- (mapOnObjects2 CC.uncoproject (src (baseCocone cc))) 
        let renameCocone cc = unsafeCocone (renamingDiag ->$ (nadir cc)) (renamingDiag <-@<= (legsCocone cc) <=@<- (unsafeInverseDiagram $ uncoprojectCocone cc))
        let renameTwoCone c = unsafeCone (renamingDiag ->$ (apex c)) (renamingDiag <-@<= (legsCone c))
        let renameExp exp = unsafeTripod (renameTwoCone $ twoCone exp) (renamingDiag ->£ (evalMap exp))
        let cones = renameCone <$> distinguishedCones colim
        let cocones = renameCocone <$> distinguishedCocones colim
        let tripods = renameExp <$> distinguishedTripods colim
        return $ unsafeSketch (tgt renamingDiag) cones cocones tripods
      
    extractInstances :: SketchStatement -> Maybe SketchStatement
    extractInstances x@(Instance _ _) = Just x
    extractInstances x@(InstanceInclusion _) = Just x
    extractInstances _ = Nothing
    
    constructInstantiationDiagram :: (FilePath,Identifier) -> IdentifierTable -> Sketch SketchObject SketchArrow -> [SketchStatement] -> Either SemanticError (Diagram (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch SketchObject SketchArrow) (SketchMorphism SketchObject SketchArrow) (Sketch SketchObject SketchArrow))
    constructInstantiationDiagram (fp,identif) idTable sketchBeforeInstance stmts = do
        let instanceStatements = catMaybes $ extractInstances <$> stmts
        let n = length instanceStatements
        let indexingCatColimit = unsafeCompositionGraph (unsafeGraph (set [0..2*n]) (set $ [Arrow{sourceArrow = i+n + 1, targetArrow = 0, labelArrow = i+1}| i <- [0..n-1]]++[Arrow{sourceArrow = i+n+1, targetArrow = i +1 , labelArrow = i+n+1}| i <- [0..n-1]])) Map.empty
        let emptyDiag = Diagram{src = indexingCatColimit, tgt = FinSketch, omap = (weakMap [(0,sketchBeforeInstance)]), mmap = Map.empty}
        diagWithClassSm <- foldr (updateDiagWithClassSM (fp,identif) idTable n) (Right emptyDiag) (zip [0..n-1] instanceStatements)
        diagWithAllSm <- foldr (updateDiagWithInstanceSM (fp,identif) n) (Right diagWithClassSm) (zip [0..n-1] instanceStatements)
        return $ completeDiagram diagWithAllSm

    isAtomicObject (Object _) = True
    isAtomicObject _ = False
    
    isAtomicArrow m = case unsafeGetLabel m of
        (ElementaryArrow _) -> True
        _ -> False

    updateDiagWithClassSM :: (FilePath, Identifier) -> IdentifierTable -> Int -> (Int,SketchStatement) -> Either SemanticError (Diagram (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch SketchObject SketchArrow) (SketchMorphism SketchObject SketchArrow) (Sketch SketchObject SketchArrow)) -> Either SemanticError (Diagram (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch SketchObject SketchArrow) (SketchMorphism SketchObject SketchArrow) (Sketch SketchObject SketchArrow))
    updateDiagWithClassSM _ _ _ _ x@(Left _) = x
    updateDiagWithClassSM (fp,identif) idTable n (i,(Instance sm _)) (Right d) = do
        semanticClassSm <- constructAnIdentifier idTable (fp,sm)
        let SemanticSketchMorphism classSm = semanticClassSm
        return $ Diagram{
            src = src d,
            tgt = tgt d,
            omap = omap d,
            mmap = Map.insert (unsafeGetMorphismFromLabel (src d) (i+n+1)) classSm (mmap d)}
    updateDiagWithClassSM (fp,identif) idTable n (i,(InstanceInclusion sm)) (Right d) = do
        semanticClassSm <- constructAnIdentifier idTable (fp,sm)
        let SemanticSketchMorphism classSm = semanticClassSm
        return $ Diagram{
            src = src d,
            tgt = tgt d,
            omap = omap d,
            mmap = Map.insert (unsafeGetMorphismFromLabel (src d) (i+n+1)) classSm (mmap d)}
            
    
    updateDiagWithInstanceSM :: (FilePath,Identifier) -> Int -> (Int,SketchStatement) -> Either SemanticError (Diagram (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch SketchObject SketchArrow) (SketchMorphism SketchObject SketchArrow) (Sketch SketchObject SketchArrow)) -> Either SemanticError (Diagram (CompositionGraph Int Int) (CGMorphism Int Int) Int (FinSketch SketchObject SketchArrow) (SketchMorphism SketchObject SketchArrow) (Sketch SketchObject SketchArrow))
    updateDiagWithInstanceSM _ _ _ x@(Left _) = x
    updateDiagWithInstanceSM (fp,identif) n (i,(Instance _ stmts)) (Right d) = do
        let maybeClassSm = (mmap d) |?| (unsafeGetMorphismFromLabel (src d) (i+n+1))
        if null maybeClassSm then error $ "no class sketch morphism when constructing " ++ show (fp,identif) else do
            let Just classSm = maybeClassSm
            let sourceSketch = source classSm
            let maybeTargetSketch = (omap d) |?| 0
            if null maybeTargetSketch then error $ "No target sketch when constructing " ++ show (fp,identif) else do
                let Just targetSketch = maybeTargetSketch
                let emptyDiag = Diagram{src = underlyingCategory sourceSketch, tgt = underlyingCategory targetSketch, omap = Map.empty, mmap = Map.empty}
                result <- completeEmptySketchMorphismDeclaration (fp,identif) stmts (unsafeSketchMorphism sourceSketch targetSketch emptyDiag)
                return $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap = Map.insert (unsafeGetMorphismFromLabel (src d) (i+1)) result (mmap d)}
    updateDiagWithInstanceSM (fp,identif) n (i,(InstanceInclusion _)) (Right d) = do
        let maybeClassSm = (mmap d) |?| (unsafeGetMorphismFromLabel (src d) (i+n+1))
        if null maybeClassSm then error $ "no class sketch morphism when constructing " ++ show (fp,identif) else do
            let Just classSm = maybeClassSm
            let sourceSketch = source classSm
            let maybeTargetSketch = (omap d) |?| 0
            if null maybeTargetSketch then error $ "No target sketch when constructing " ++ show (fp,identif) else do
                let Just targetSketch = maybeTargetSketch
                let emptyDiag = Diagram{src = underlyingCategory sourceSketch, tgt = underlyingCategory targetSketch, omap = Map.empty, mmap = Map.empty}
                result <- completeEmptySketchMorphismInclusion (fp,identif) (unsafeSketchMorphism sourceSketch targetSketch emptyDiag)
                return $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap = Map.insert (unsafeGetMorphismFromLabel (src d) (i+1)) result (mmap d)}
                
        
        
    -- | Construct a 'SemanticSketch', should be called by constructAnIdentifier.
    constructSemanticSketch :: IdentifierTable -> (FilePath, Identifier) -> Either SemanticError SemanticToken
    constructSemanticSketch idTable (fp,i) = do
        let (InternalRessource (Sketch (SketchDeclaration _ stmts))) = idTable |!| (fp,i)
        objectsToCreate <- constructSketchObjects (fp,i) stmts
        arrowsToCreate <- constructSketchArrows (fp,i) objectsToCreate stmts
        lawToCreate <- constructCompositionLaw (fp,i) objectsToCreate arrowsToCreate stmts
        let cg =  unsafeCompositionGraph (unsafeGraph objectsToCreate arrowsToCreate) lawToCreate
        checkEq <- sequence $ checkEqualizers (fp,i) objectsToCreate arrowsToCreate <$> stmts
        c <- constructCones (fp,i) cg stmts
        cc <- constructCocones (fp,i) cg stmts
        t <- constructTripods (fp,i) cg
        let sketchBeforeInstance = unsafeSketch cg c cc t
        finalSketch <- instantiate (fp,i) idTable sketchBeforeInstance stmts
        return $ simplify $ SemanticSketch finalSketch
            
           

















           
            
    -- * SEMANTIC SKETCH MORPHISM
    
    updateDiagramWithSketchMorphismStatement :: (FilePath,Identifier) -> SketchMorphismStatement -> Either SemanticError (FunctorSketch SketchObject SketchArrow) ->  Either SemanticError (FunctorSketch SketchObject SketchArrow)
    updateDiagramWithSketchMorphismStatement _ _ (Left x) = (Left x)
    updateDiagramWithSketchMorphismStatement (fp,i) (MapObject x y) (Right d) = Right $ Diagram{
            src = src d,
            tgt = tgt d,
            omap = Map.insert (parsedObjectToSketchObject (ob (src d)) x) (parsedObjectToSketchObject (ob (tgt d)) y) (omap d),
            mmap = mmap d
        }
    updateDiagramWithSketchMorphismStatement (fp,i) (MapArrow x y) (Right d) = do
        antecedantMorph <- (parsedLabelArrowToCGMorphism (fp,i) (src d) x)
        imageMorph <- compose <$> reverse <$> (sequence $ parsedLabelArrowToCGMorphism (fp,i) (tgt d) <$> y)
        return Diagram{
        src = src d,
        tgt = tgt d,
        omap = omap d,
        mmap = Map.insert antecedantMorph imageMorph (mmap d)
        }
    
    recursiveObjectDependencies :: CompositionGraph SketchObject SketchArrow -> Set SketchObject -> SketchObject -> Maybe [SketchObject]
    recursiveObjectDependencies cg blacklist (Object x)
        | obj `isIn` blacklist = Just []
        | obj `isIn` ob cg = Nothing
        | otherwise = (obj:) <$> recursiveObjectDependencies cg (Set.insert obj blacklist) obj
        where
            obj = parsedObjectToSketchObject (ob cg) (Object x)
    recursiveObjectDependencies cg blacklist (TerminalObject _) = Nothing
    recursiveObjectDependencies cg blacklist (InitialObject _) = Nothing
    recursiveObjectDependencies cg blacklist x@(Product _ xs) = (obj:) <$> (listToMaybe $ Data.Maybe.catMaybes $ (recursiveObjectDependencies cg (Set.insert obj blacklist)) <$> xs)
        where
            obj = parsedObjectToSketchObject (ob cg) x
    recursiveObjectDependencies cg blacklist x@(Coproduct _ xs) = (obj:) <$> (listToMaybe $ Data.Maybe.catMaybes $ (recursiveObjectDependencies cg (Set.insert obj blacklist)) <$> xs)
        where
            obj = parsedObjectToSketchObject (ob cg) x
    recursiveObjectDependencies cg blacklist x@(Power _ dom codom) = (obj:) <$> (listToMaybe $ Data.Maybe.catMaybes $ (recursiveObjectDependencies cg (Set.insert obj blacklist)) <$> [dom,codom])
        where
            obj = parsedObjectToSketchObject (ob cg) x
    
    isRecursiveObject :: CompositionGraph SketchObject SketchArrow -> SketchObject -> Bool
    isRecursiveObject cg obj = not $ null $ recursiveObjectDependencies cg (set []) obj
    
    imageInducedObject :: (FilePath,Identifier) -> FunctorSketch SketchObject SketchArrow -> SketchObject -> Either SemanticError SketchObject
    imageInducedObject (fp,i) d (Object x)
        | (Object x) `isIn` (keys' $ omap d) = Right $ d ->$ (Object x)
        | otherwise = Left $ SketchMorphismErrorMissingObjectImage fp i (Object x)
    imageInducedObject (fp,i) d (TerminalObject xs)
        | not $ imageTerminal `isIn` (ob $ tgt d) = Left $ SketchMorphismErrorMissingObjectImage fp i (TerminalObject xs)
        | otherwise = Right $ imageTerminal
        where
            imageTerminal = parsedObjectToSketchObject (ob $ tgt d) (TerminalObject (set []))
    imageInducedObject (fp,i) d (InitialObject xs)
        | not $ imageInitial `isIn` (ob $ tgt d) = Left $ SketchMorphismErrorMissingObjectImage fp i (InitialObject xs)
        | otherwise = Right $ imageInitial
        where
            imageInitial = parsedObjectToSketchObject (ob $ tgt d) (InitialObject (set []))
    imageInducedObject (fp,i) d x@(Product aliases xs) = do
        imgs <- sequence (imageInducedObject (fp,i) d <$> xs)
        let imgProd = parsedObjectToSketchObject (ob $ tgt d) (Product (set []) imgs)
        if not $ imgProd `isIn` (ob $ tgt d) then Left $ SketchMorphismErrorMissingObjectImage fp i x else do
            return $ imgProd
    imageInducedObject (fp,i) d x@(Coproduct aliases xs) = do
        imgs <- sequence (imageInducedObject (fp,i) d <$> xs)
        let imgCoprod = parsedObjectToSketchObject (ob $ tgt d) (Coproduct (set []) imgs)
        if not $ imgCoprod `isIn` (ob $ tgt d) then Left $ SketchMorphismErrorMissingObjectImage fp i x else do
            return $ imgCoprod
    imageInducedObject (fp,i) d x@(Power aliases dom codom) = do
        imgDom <- imageInducedObject (fp,i) d dom
        imgCodom <- imageInducedObject (fp,i) d codom
        let imgPow = parsedObjectToSketchObject (ob $ tgt d) (Power (set []) imgDom imgCodom)
        if not $ imgPow `isIn` (ob $ tgt d) then Left $ SketchMorphismErrorMissingObjectImage fp i x else do
            return $ imgPow
    
    completeDiagramWithInducedObjectMapping :: (FilePath,Identifier) -> SketchObject -> Either SemanticError (FunctorSketch SketchObject SketchArrow) -> Either SemanticError (FunctorSketch SketchObject SketchArrow)
    completeDiagramWithInducedObjectMapping _ _ x@(Left _) = x
    completeDiagramWithInducedObjectMapping (fp,i) obj (Right d) = do
        if not $ null $ (omap d) |?| obj then return d else do
            if isRecursiveObject (src d) obj then return d else do
                img <- imageInducedObject (fp,i) d obj
                return $ Diagram{src = src d, tgt = tgt d, omap = Map.insert obj img (omap d), mmap = mmap d}
            
    -- | completeDiagramWithInducedObjectMapping should already have been called
    completeDiagramWithInducedArrowMapping :: (FilePath,Identifier) -> SketchObject -> Either SemanticError (FunctorSketch SketchObject SketchArrow) -> Either SemanticError (FunctorSketch SketchObject SketchArrow)
    completeDiagramWithInducedArrowMapping _ _ x@(Left _) = x
    completeDiagramWithInducedArrowMapping _ (Object x) (Right d) = Right d
    completeDiagramWithInducedArrowMapping _ (TerminalObject _) (Right d) = Right d
    completeDiagramWithInducedArrowMapping _ (InitialObject _) (Right d) = Right d
    completeDiagramWithInducedArrowMapping (fp,i) x@(Product _ xs) (Right d)
        | null maybeMap = Left $ SketchMorphismErrorMissingArrowImage fp i $ head [(Projection x i) | i <- [0..(length xs - 1)], null $ getMorphismFromLabel (tgt d) (Projection (d ->$ x) i)]
        | otherwise = Right Diagram{src = src d, tgt = tgt d, omap = omap d, mmap = mmap d `Map.union` newMap}
        where
            maybeMap = weakMap <$> sequence [sequence (unsafeGetMorphismFromLabel (src d) (Projection x i),getMorphismFromLabel (tgt d) (Projection (d ->$ x) i)) | i <- [0..(length xs - 1)]]
            Just newMap = maybeMap
    completeDiagramWithInducedArrowMapping (fp,i) x@(Coproduct _ xs) (Right d)
        | null maybeMap = Left $ SketchMorphismErrorMissingArrowImage fp i $ head [(Coprojection x i) | i <- [0..(length xs - 1)], null $ getMorphismFromLabel (tgt d) (Coprojection (d ->$ x) i)]
        | otherwise = Right Diagram{src = src d, tgt = tgt d, omap = omap d, mmap = mmap d `Map.union` newMap}
        where
            maybeMap = weakMap <$> sequence [sequence (unsafeGetMorphismFromLabel (src d) (Coprojection x i),getMorphismFromLabel (tgt d) (Coprojection (d ->$ x) i)) | i <- [0..(length xs - 1)]]
            Just newMap = maybeMap
    completeDiagramWithInducedArrowMapping (fp,i) x@(Power _ dom codom) (Right d)
        | null maybeMap = Left $ SketchMorphismErrorMissingArrowImage fp i $ EvalMap srcProd 
        | otherwise = Right $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap = mmap d `Map.union` newMap}
        where
            srcProd = parsedObjectToSketchObject (ob $ src d) (Product (set []) [x,dom])
            tgtProd = parsedObjectToSketchObject (ob $ tgt d) (Product (set []) [if null (omap d |?| x) then error "image not constructed yet" else d ->$ x, if null (omap d |?| dom) then error "image not constructed yet" else d ->$ dom])
            maybeMap = weakMap <$> sequence [sequence (unsafeGetMorphismFromLabel (src d) (EvalMap srcProd),getMorphismFromLabel (tgt d) (EvalMap tgtProd))]
            Just newMap = maybeMap
    
    checkMissingObjectImage :: (FilePath,Identifier) -> FunctorSketch SketchObject SketchArrow -> Either SemanticError ()
    checkMissingObjectImage (fp,i) diag
        | Set.null $ ob (src diag) |-| keys' (omap diag) = Right ()
        | otherwise = Left $ SketchMorphismErrorMissingObjectImage fp i $ (anElement $ ob (src diag) |-| keys' (omap diag))
    
    completeEmptySketchMorphismDeclaration :: (FilePath,Identifier) -> [SketchMorphismStatement] -> SketchMorphism SketchObject SketchArrow -> Either SemanticError (SketchMorphism SketchObject SketchArrow)
    completeEmptySketchMorphismDeclaration (fp,identif) stmts emptySM = do
        let emptyDiag = underlyingFunctor emptySM
        atomicDiag <- foldr (updateDiagramWithSketchMorphismStatement (fp,identif)) (Right emptyDiag) stmts
        diagWithConstructedObjects <- Set.foldr (completeDiagramWithInducedObjectMapping (fp,identif)) (Right atomicDiag) (ob $ src atomicDiag)
        errMissingObjectImage <- checkMissingObjectImage (fp,identif) diagWithConstructedObjects
        diagWithConstructedArrows <- Set.foldr (completeDiagramWithInducedArrowMapping (fp,identif)) (Right $ completeDiagram diagWithConstructedObjects) (ob $ src diagWithConstructedObjects)
        let result = sketchMorphism (source emptySM) (target emptySM) diagWithConstructedArrows
        if null result then do
            let Left errSM = result
            Left $ SketchMorphismDeclarationError fp identif errSM
        else do
            let Right sm = result
            return sm
          
    completeDiagramWithObjectInclusion :: (FilePath,Identifier) -> SketchObject -> Either SemanticError (FunctorSketch SketchObject SketchArrow) -> Either SemanticError (FunctorSketch SketchObject SketchArrow)
    completeDiagramWithObjectInclusion _ _ x@(Left _) = x
    completeDiagramWithObjectInclusion (fp,i) obj (Right d)
        | imgObj `isIn` (ob $ tgt d) = Right $ Diagram{src = src d, tgt = tgt d, omap = Map.insert obj imgObj (omap d), mmap = mmap d}
        | otherwise = Left $ SketchMorphismInclusionErrorObjectNotInImage fp i obj
        where
            imgObj = parsedObjectToSketchObject (ob $ tgt d) obj
          
    completeDiagramWithArrowInclusion :: (FilePath,Identifier) -> SketchArrow -> Either SemanticError (FunctorSketch SketchObject SketchArrow) -> Either SemanticError (FunctorSketch SketchObject SketchArrow)
    completeDiagramWithArrowInclusion _ _ x@(Left _) = x
    completeDiagramWithArrowInclusion (fp,identif) x@(ElementaryArrow _) (Right d)
        | null maybeImgArr = Left $ SketchMorphismInclusionErrorArrowNotInImage fp identif x
        | otherwise = Right $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap =  Map.insert (unsafeGetMorphismFromLabel (src d) x) imgArr (mmap d)}
        where
            maybeImgArr = getMorphismFromLabel (tgt d) x
            Just imgArr = maybeImgArr
    completeDiagramWithArrowInclusion (fp,identif) x@(Projection prod i) (Right d)
        | null maybeImgArr = Left $ SketchMorphismInclusionErrorArrowNotInImage fp identif x
        | otherwise = Right $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap =  Map.insert (unsafeGetMorphismFromLabel (src d) x) imgArr (mmap d)}
        where
            maybeImgArr = getMorphismFromLabel (tgt d) (Projection (d ->$ prod) i)
            Just imgArr = maybeImgArr
    completeDiagramWithArrowInclusion (fp,identif) x@(Coprojection prod i) (Right d)
        | null maybeImgArr = Left $ SketchMorphismInclusionErrorArrowNotInImage fp identif x
        | otherwise = Right $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap =  Map.insert (unsafeGetMorphismFromLabel (src d) x) imgArr (mmap d)}
        where
            maybeImgArr = getMorphismFromLabel (tgt d) (Coprojection (d ->$ prod) i)
            Just imgArr = maybeImgArr
    completeDiagramWithArrowInclusion (fp,identif) x@(EvalMap pow) (Right d)
        | null maybeImgArr = Left $ SketchMorphismInclusionErrorArrowNotInImage fp identif x
        | otherwise = Right $ Diagram{src = src d, tgt = tgt d, omap = omap d, mmap =  Map.insert (unsafeGetMorphismFromLabel (src d) x) imgArr (mmap d)}
        where
            maybeImgArr = getMorphismFromLabel (tgt d) (EvalMap (d ->$ pow))
            Just imgArr = maybeImgArr
          
    
    completeEmptySketchMorphismInclusion :: (FilePath,Identifier) -> SketchMorphism SketchObject SketchArrow -> Either SemanticError (SketchMorphism SketchObject SketchArrow)
    completeEmptySketchMorphismInclusion (fp,identif) emptySM = do
        let emptyDiag = underlyingFunctor emptySM
        diagWithObjects <- Set.foldr (completeDiagramWithObjectInclusion (fp,identif)) (Right emptyDiag) (ob $ src emptyDiag)
        diagWithArrows <- Set.foldr (completeDiagramWithArrowInclusion (fp,identif)) (Right diagWithObjects) (unsafeGetLabel <$> (genArrowsWithoutId $ src diagWithObjects))
        let result = sketchMorphism (source emptySM) (target emptySM) (completeDiagram diagWithArrows)
        if null result then do
            let Left errSM = result
            Left $ SketchMorphismInclusionError fp identif errSM
        else do
            let Right sm = result
            return sm
    
    -- | Construct a 'SemanticSketchMorphism', should be called by constructAnIdentifier.
    constructSemanticSketchMorphism :: IdentifierTable -> (FilePath, Identifier) -> Either SemanticError SemanticToken
    constructSemanticSketchMorphism idTable (fp,i) = case token of
        (InternalRessource (SketchMorphism (SketchMorphismDeclaration _ s t stmts))) -> do
            semanticSourceSketch <- constructAnIdentifier idTable (fp,s)
            let (SemanticSketch sourceSketch) = semanticSourceSketch
            semanticTargetSketch <- constructAnIdentifier idTable (fp,t)
            let (SemanticSketch targetSketch) = semanticTargetSketch
            let emptyDiag = Diagram{src = underlyingCategory sourceSketch, tgt = underlyingCategory targetSketch, omap = Map.empty, mmap = Map.empty}
            result <- completeEmptySketchMorphismDeclaration (fp,i) stmts (unsafeSketchMorphism sourceSketch targetSketch emptyDiag)
            return $ SemanticSketchMorphism result
        (InternalRessource (SketchMorphism (SketchMorphismInclusion _ s t))) -> do
            semanticSourceSketch <- constructAnIdentifier idTable (fp,s)
            let (SemanticSketch sourceSketch) = semanticSourceSketch
            semanticTargetSketch <- constructAnIdentifier idTable (fp,t)
            let (SemanticSketch targetSketch) = semanticTargetSketch
            let emptyDiag = Diagram{src = underlyingCategory sourceSketch, tgt = underlyingCategory targetSketch, omap = Map.empty, mmap = Map.empty}
            result <- completeEmptySketchMorphismInclusion (fp,i) (unsafeSketchMorphism sourceSketch targetSketch emptyDiag)
            return $ SemanticSketchMorphism result
        where
            token = idTable |!| (fp,i)
            
                
                
                
                
             
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
    -- * SEMANTIC MODEL
    
    -- | Construct a 'SemanticModel', should be called by constructAnIdentifier.
    constructSemanticModel :: IdentifierTable -> (FilePath, Identifier) -> Either SemanticError SemanticToken
    constructSemanticModel idTable (fp,i)
        | null errOrModels = Left $ err1
        | not $ null differentLang = Left $ (uncurry $ ModelErrorInstancesOfDifferentLanguages fp i) (head differentLang)
        | otherwise = Right $ SemanticModel lang (foldr (<>) "" (extractText <$> models))
        where
            (InternalRessource (Model (ModelDeclaration _ sketch lang instances txt))) = idTable |!| (fp,i)
            errOrModels = sequence $ (\x -> constructAnIdentifier idTable (fp,x)) <$> instances
            Left err1 = errOrModels
            Right modelsWithoutTheNonInst = errOrModels
            models = (SemanticModel lang txt) : modelsWithoutTheNonInst
            extractLang (SemanticModel l _) = l
            extractText (SemanticModel _ t) = t
            langs = zip instances (extractLang <$> models)
            differentLang = [((a,l1),(b,l2)) | (a,l1) <- langs, (b,l2) <- langs, l1 /= l2]