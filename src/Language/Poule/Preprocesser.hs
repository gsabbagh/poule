{-# LANGUAGE MonadComprehensions #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

{-| Module  : poule
Description : Preprocesser for poule.
Copyright   : Guillaume Sabbagh 2024
License     : LGPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Preprocess poule files before the semantic analysis. Handles imports.

-}

module Language.Poule.Preprocesser
(
    Identifier(..),
    Ressource(..),
    PreprocessError(..),
    IdentifierTable(..),
    preprocessPouleFile,
)

where
    import              Language.Poule.Parser  as P
    
    import              Text.Megaparsec     hiding  (try, ParseError)
    
    import              Math.IO.PrettyPrint
    
    import              Data.List                   (sortBy)
    import              Data.WeakSet                (Set)
    import qualified    Data.WeakSet            as  Set
    import              Data.WeakSet.Safe
    import              Data.WeakMap                (Map)
    import qualified    Data.WeakMap            as  Map
    import              Data.WeakMap.Safe
    import              Data.Simplifiable
    import              Data.Maybe                  (fromJust)
    import qualified    Data.Text               as  T
    import              Data.Text.IO                (readFile)
    import              Data.Void
    
    import              GHC.Generics
    
    import              Control.Exception
    
    type Identifier = T.Text
    
    -- | An error occuring during the preprocessing.
    data PreprocessError = CouldNotFindFile FilePath
                         | DuplicateIdentifiers FilePath [((Identifier, Ressource),(Identifier, Ressource))]
                         | ParseError FilePath PouleParseError
                        deriving (Show, Eq, Generic, PrettyPrint, Simplifiable)
                        
    -- | The file table which maps each file path to its parsed tokens.
    type FileTable = Map FilePath ParsedPouleFile
    
    -- | Given an input file, construct its file table.
    constructFileTable :: FilePath -> IO (Either PreprocessError FileTable)
    constructFileTable fp = constructFileTableHelper (weakMap []) fp
            
    constructFileTableHelper :: FileTable -> FilePath -> IO (Either PreprocessError FileTable)
    constructFileTableHelper ft fp 
        | fp `isIn` keys' ft = return $ Right ft
        | otherwise = do
            -- Open the file
            fileOrError <- (try $ Data.Text.IO.readFile fp) :: IO (Either SomeException T.Text)
            if null fileOrError then return $ Left $ CouldNotFindFile fp else do 
                let Right file = fileOrError
                let fileWithEmptyLineAtTheEnd = file <> (T.pack ";")
                -- Parse the file
                let parsedTokensOrError = parse parsePoule "" fileWithEmptyLineAtTheEnd
                if null parsedTokensOrError then do 
                    let Left err = parsedTokensOrError
                    return $ Left $ ParseError fp (PouleParseError err)
                else do
                    let Right parsedTokens = parsedTokensOrError
                    let newFt = Map.insert fp parsedTokens ft
                    let filesToImport = concat $ extractFilePathFromImportStatement <$> parsedTokens
                    -- Recursive call on all imports
                    imports <- sequence $ constructFileTableHelper newFt <$> fp:filesToImport
                    let errOrFinalFTS = sequence imports
                    if null errOrFinalFTS then do
                        let Left err = errOrFinalFTS
                        return $ Left $ err
                    else do
                        return $ (simplify.(Map.unions)) <$> errOrFinalFTS
        where
            extractFilePathFromImportStatement (Import (FromImportAsStatement f _ _)) = [f]
            extractFilePathFromImportStatement (Import (ImportHidingStatement f _)) = [f]
            extractFilePathFromImportStatement (Import (FromExportAsStatement f _ _)) = [f]
            extractFilePathFromImportStatement (Import (ExportHidingStatement f _)) = [f]
            extractFilePathFromImportStatement _ = []
                
    -- | A ressource is either an internal or an external ressource.
    data Ressource = InternalRessource PouleToken | ExternalRessource FilePath Identifier deriving (Eq, Show, Generic, PrettyPrint, Simplifiable)
    
    isExternal :: Ressource -> Bool
    isExternal (ExternalRessource _ _) = True
    isExternal (InternalRessource _) = False
                
    -- | Table of identifier which maps a file path to the set of its public/private identifiers.
    type IdentifierTable = Map (FilePath,Identifier) Ressource
    
    -- | Take a list l and a function f and return every element x of l such that another element y of l has the same image as x by f.
    duplicates :: (Ord b) => [a] -> (a -> b) -> [(a,a)]
    duplicates xs f = dupl mapping
        where
            mapping = sortBy (\(x,_) (y,_) -> compare x y) $ zip (f <$> xs) xs
            dupl [] = []
            dupl (_:[]) = []
            dupl (x1:(x2:xs)) = if (fst x1) == (fst x2) then ((snd x1, snd x2):(dupl (x2:xs))) else dupl (x2:xs)
    
    -- | Given a file table and a black list of file paths, returns all public identifiers a file.
    getPublicIdentifiers :: FileTable -> Set FilePath -> FilePath -> [(Identifier,Ressource)]
    getPublicIdentifiers ft blacklist fp
        | fp `isIn` blacklist = []
        | otherwise = concat $ extractPublicIdentifier <$> (ft |!| fp)
        where
            extractPublicIdentifier (Import (FromImportAsStatement _ _ _)) = []
            extractPublicIdentifier (Import (ImportHidingStatement _ _)) = []
            extractPublicIdentifier (Import (FromExportAsStatement f x y)) = [(y,ExternalRessource f x)]
            extractPublicIdentifier (Import (ExportHidingStatement f hides)) = [(i, ExternalRessource f i) | (i,r) <- getPublicIdentifiers ft (Set.insert fp blacklist) f, not $ i `elem` hides]
            extractPublicIdentifier t@(Sketch (SketchDeclaration name _)) = [(name, InternalRessource t)]
            extractPublicIdentifier t@(SketchMorphism (SketchMorphismDeclaration name _ _ _)) =  [(name, InternalRessource t)]
            extractPublicIdentifier t@(SketchMorphism (SketchMorphismInclusion name _ _)) = [(name, InternalRessource t)]
            extractPublicIdentifier t@(Model (ModelDeclaration name _ _ _ _)) = [(name, InternalRessource t)]
    
    -- | Construct the table of public identifiers.
    constructPublicIdentifierTable :: FileTable -> Either PreprocessError IdentifierTable
    constructPublicIdentifierTable ft = weakMap <$> (concat <$> (sequence $ checkForDuplicates <$> publicIdentifiers))
        where
            publicIdentifiers = [(x,getPublicIdentifiers ft (set []) x) | x <- (Map.keys ft)]
            checkForDuplicates (fp,l)
                | duplicates l fst /= [] = Left $ DuplicateIdentifiers fp $ duplicates l fst
                | otherwise = Right $ (\(i,r) -> ((fp,i),r)) <$> l
                
    -- | Given a file table and a black list of file paths, returns all private identifiers a file.
    getPrivateIdentifiers :: FileTable -> Set FilePath -> FilePath -> [(Identifier,Ressource)]
    getPrivateIdentifiers ft blacklist fp
        | fp `isIn` blacklist = []
        | otherwise = concat $ extractPrivateIdentifier <$> (ft |!| fp)
        where
            extractPrivateIdentifier (Import (FromImportAsStatement f x y)) = [(y,ExternalRessource f x)]
            extractPrivateIdentifier (Import (ImportHidingStatement f hides)) = [(i, ExternalRessource f i) | (i,r) <- getPublicIdentifiers ft (Set.insert fp blacklist) f, not $ i `elem` hides]
            extractPrivateIdentifier (Import (FromExportAsStatement f x y)) = []
            extractPrivateIdentifier (Import (ExportHidingStatement f hides)) = []
            extractPrivateIdentifier t@(Sketch (SketchDeclaration name _)) = []
            extractPrivateIdentifier t@(SketchMorphism (SketchMorphismDeclaration name _ _ _)) =  []
            extractPrivateIdentifier t@(SketchMorphism (SketchMorphismInclusion name _ _)) = []
            extractPrivateIdentifier t@(Model (ModelDeclaration name _ _ _ _)) = []
    
    -- | Construct the table of public identifiers.
    constructPrivateIdentifierTable :: FileTable -> Either PreprocessError IdentifierTable
    constructPrivateIdentifierTable ft = weakMap <$> (concat <$> (sequence $ checkForDuplicates <$> privateIdentifiers))
        where
            privateIdentifiers = [(x,getPrivateIdentifiers ft (set []) x) | x <- (Map.keys ft)]
            checkForDuplicates (fp,l)
                | duplicates l fst /= [] = Left $ DuplicateIdentifiers fp $ duplicates l fst
                | otherwise = Right $ (\(i,r) -> ((fp,i),r)) <$> l
    
       -- | Preprocess a Poule file.
    preprocessPouleFile :: FilePath -> IO (Either PreprocessError IdentifierTable)
    preprocessPouleFile fp = do
        ftOrErr <- constructFileTable fp
        return $ do  
            ft <- ftOrErr
            pub <- constructPublicIdentifierTable ft
            priv <- constructPrivateIdentifierTable ft
            if not $ Set.null $ keys' pub |&| keys' priv then do
                let (f,i) = anElement $ keys' pub |&| keys' priv
                Left $ DuplicateIdentifiers f [((i,pub |!| (f,i)), (i,priv |!| (f,i)))]
            else do 
                return $ Map.union pub priv